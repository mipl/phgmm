import argparse
import datetime
import importlib
import os
import numpy as np
import re
import tensorflow as tf
import yaml
from dataset.helper import *
from models.PHGMM import PHGMM
from summary.summary import CTensorBoard

from dataset.Cityscape_read import CITYSCAPES
from dataset.Synthia_read import SYNTHIA

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-c', '--config', default='config/cityscapes_train.config')

tf.reset_default_graph()

def _tower_loss(model, images, label_pi, label_S, batch_size, scope, reuse_variables=None):
  """Calculate the total loss on a single tower running the Network model.
  We perform 'batch splitting'. This means that we cut up a batch across
  multiple GPUs. For instance, if the batch size = 32 and num_gpus = 2,
  then each tower will operate on an batch of 16 images.
  Args:
    images: Images. 4D tensor of size [batch_size, FLAGS.image_size, FLAGS.image_size, 3].
    labels: 1-D integer Tensor of [batch_size].
    num_classes: number of classes
    scope: unique prefix string identifying the ImageNet tower, e.g.
      'tower_0'.
  Returns:
     Tensor of shape [] containing the total loss for a batch of data
  """

  # Build inference Graph.
  with tf.variable_scope(tf.get_variable_scope(), reuse=reuse_variables):
    prob_gmm, z_prior_mu, z_prior_var, g_mu, g_var, S, aux1_S, aux2_S, \
          = model.forward(images, batch_size)
    
    #z_pos_mu = None; z_pos_var = None;
    z_pos_mu, z_pos_var = model.postNet(data=images, label_S=label_S, name='postNet')

  # Build the portion of the Graph calculating the losses. Note that we will
  # assemble the total_loss using a custom function below.  
  loss, loss_g, loss_prob_gmm, loss_z, loss_S, loss_S_iou, = \
      model.loss(prob_gmm_pred=prob_gmm, prob_gmm_label=label_pi, \
            prior_z_mu=z_prior_mu, prior_z_var=z_prior_var, pos_z_mu=z_pos_mu, pos_z_var=z_pos_var, \
            g_mu=g_mu, g_var=g_var, batch_sz=batch_size, \
            logits_ss=S, label_ss=label_S, aux1_ss=aux1_S, aux2_ss=aux2_S)

  # Assemble all of the losses for the current tower only.
  losses = tf.get_collection('losses', scope)

  # Calculate the total loss for the current tower.
  total_loss = tf.add_n(losses, name='total_loss')

  # Compute the moving average of all individual losses and the total loss.
  loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
  loss_averages_op = loss_averages.apply(losses + [total_loss])

  # Attach a scalar summmary to all individual losses and the total loss; do the
  # same for the averaged version of the losses.
  with tf.control_dependencies([loss_averages_op]):
    total_loss = tf.identity(total_loss)

  return total_loss, S, loss_prob_gmm, loss_z, loss_g, loss_S, loss_S_iou

def _average_gradients(tower_grads):
  """Calculate the average gradient for each shared variable across all towers.
  Note that this function provides a synchronization point across all towers.
  Args:
    tower_grads: List of lists of (gradient, variable) tuples. The outer list
      is over individual gradients. The inner list is over the gradient
      calculation for each tower.
  Returns:
     List of pairs of (gradient, variable) where the gradient has been averaged
     across all towers.
  """
  average_grads = []
  for grad_and_vars in zip(*tower_grads):
    # Note that each grad_and_vars looks like the following:
    #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
    grads = []
    for g, v in grad_and_vars:
      #print("------------> ", v.name, v.get_shape().as_list())
      # Add 0 dimension to the gradients to represent the tower.
      expanded_g = tf.expand_dims(g, 0)

      # Append on a 'tower' dimension which we will average over below.
      grads.append(expanded_g)

    # Average over the 'tower' dimension.
    grad = tf.concat(axis=0, values=grads)
    grad = tf.reduce_mean(grad, 0)

    # Keep in mind that the Variables are redundant because they are shared
    # across towers. So .. we will just return the first tower's pointer to
    # the Variable.
    v = grad_and_vars[0][1]
    grad_and_var = (grad, v)
    average_grads.append(grad_and_var)
  return average_grads

def get_variables_to_restore(variables):
  variables_to_restore = []
  for v in variables:
    #print("--> v.name: ", v.name.split(':')[0])
    # one can do include or exclude operations here.
    if 'ExponentialMovingAverage' not in v.name.split(':')[0] and \
       '/avg' not in v.name.split(':')[0]  and '_power' not in v.name.split(':')[0] \
        and '/Adam' not in v.name.split(':')[0]:
      #print("Variables restored: %s" % v.name)
      variables_to_restore.append(v)
  return variables_to_restore

def get_variables_to_train(self, variables, frozen=False):
  """Returns a list of variables to train.
  Returns:
    A list of variables to train by the optimizer.
  """
  variables_to_train = []

  no_train_var=[]
  for var_train in variables:
    var_name_complete = str(var_train.name).split(':')[0]
    name_var = str(var_train.name).split(':')[0].split('/')
    name = name_var[0]
    if frozen:
      if name in no_train_var:
        continue
    variables_to_train.append(var_train)
  return variables_to_train

def train_func(config):
  """Train on dataset for a number of steps."""

  tf.reset_default_graph()
  with tf.Graph().as_default(), tf.device('/cpu:0'):
    #data_bt, label_bt, bound_bt, contourclass_bt, energy_bt, iterator = get_train_data(config)

    os.environ['CUDA_VISIBLE_DEVICES'] = config['gpu_id']
    #module = importlib.import_module('models.'+config['model'])
    #model_func = getattr(module, config['model'])
    # data_bt, label_bt, bound_bt, contourclass_bt, energy_bt, iterator = get_train_data(config)
    name_model = config['model'] 

    # Create a variable to count the number of train() calls. This equals the
    # number of batches processed * FLAGS.num_gpus.
    #global_step = tf.get_variable('Global_Step',[],initializer=tf.constant_initializer(0),trainable=False)
    global_step = tf.Variable(0, trainable=False, name='Global_Step')

    # placeholders
    batch_size_pl = tf.placeholder(tf.int64, shape=[], name="batch_size")
    images_pl = tf.placeholder(tf.float32, [None, config['height'], config['width'], 3])
    labels_pi_pl = tf.placeholder(tf.int32, [None, config['num_classes']])
    labels_S_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_S_pl = tf.one_hot(tf.cast(tf.squeeze(labels_S_pl1,axis=-1),tf.int64), config['num_classes'])

    with tf.variable_scope(name_model):
      model = PHGMM(num_classes=config['num_classes'], \
               learning_rate=config['learning_rate'], float_type=tf.float32, weight_decay=0.0005, \
               decay_steps=30000, power=config['power'], training=True, ignore_label=True, \
               global_step=global_step, has_aux_loss=True)
    
    # Create an optimizer that performs gradient descent.
    # decayed_learning_rate = learning_rate * decay_rate ^ (global_step / decay_steps)
    # decay from 0.1 to 0.01 in 10000 steps using sqrt (i.e. power=0.5)
    lr = tf.train.polynomial_decay(config['learning_rate'], \
          global_step, config['decay_steps'], config['end_learning_rate'], power=config['power'])

    opt = tf.train.AdamOptimizer(learning_rate=lr, epsilon=0.0001)

    # devices (gpus using)
    devices = []
    for i in range(len(config['gpu_id'].split(','))):
      print("/gpu:"+str(i))
      devices.append("/gpu:"+str(i))

    # Split the batch of images and labels for towers.
    images_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=images_pl)
    labels_S_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=labels_S_pl)
    label_pi_splits = tf.split(axis=0, num_or_size_splits=len(devices), value=labels_pi_pl)

    # Calculate the gradients for each model tower.
    tower_grads = []
    tower_losses = []
    tower_loss_S_cross = []
    tower_loss_S_iou = []
    
    reuse_variables = None
    logits_S = None

    batch_size = tf.cast(tf.divide(batch_size_pl,len(devices)), tf.int64)
    
    with tf.variable_scope(name_model):
      for gpu_index, gpu_device in enumerate(devices):
        with tf.device(gpu_device):
          with tf.name_scope('%s_%d' % ('tower', gpu_index)) as scope:

            # Calculate the loss for one tower of the Network model. This
            # function constructs the entire Network model but shares the
            # variables across all towers.
            loss, S, loss_prob_gmm, loss_z, loss_g, loss_S, loss_S_iou = \
                _tower_loss(model, images_splits[gpu_index], label_pi_splits[gpu_index], labels_S_splits[gpu_index], \
                  batch_size, scope, reuse_variables)

            #logist tensors output
            if logits_S is None:
              logits_S = S
            else:
              logits_S = tf.concat([logits_S, S], axis=0)

            # Reuse variables for the next tower.
            reuse_variables = True

            # Retain the Batch Normalization updates operations only from the
            # final tower. Ideally, we should grab the updates from all towers
            # but these stats accumulate extremely fast so we can ignore the
            # other stats from the other towers without significant detriment.
            batchnorm_updates = tf.get_collection('_update_ops_', scope)

            # Calculate the gradients for the batch of data on this Network tower
            grads = opt.compute_gradients(loss)

            # Keep track of the gradients across all towers.
            tower_grads.append(grads)
            tower_losses.append(loss)

            tower_loss_S_cross.append(loss_S)
            tower_loss_S_iou.append(loss_S_iou)

    # We must calculate the mean of each gradient. Note that this is the
    # synchronization point across all towers.
    grads_ave = _average_gradients(tower_grads)
    #grads_ave_clip = [(tf.clip_by_value(grad_i, -1., 1.),var_i) for grad_i, var_i in grads_ave]

    #average of loss
    tower_losses = tf.reduce_mean(tower_losses)

    # Apply the gradients to adjust the shared variables.
    apply_gradient_op = opt.apply_gradients(grads_ave, global_step=global_step)

    # Track the moving averages of all trainable variables.
    # Note that we maintain a "double-average" of the BatchNormalization
    # global statistics. This is more complicated then need be but we employ
    # this for backward-compatibility with our previous models.
    variable_averages = tf.train.ExponentialMovingAverage(0.9999, global_step)

    variables_to_average = (tf.trainable_variables() + tf.moving_average_variables())
    variables_averages_op = variable_averages.apply(variables_to_average)

    # Group all updates to into a single train op.
    batchnorm_updates_op = tf.group(*batchnorm_updates)
    train_op = tf.group(apply_gradient_op, variables_averages_op, batchnorm_updates_op)

    # Build an initialization operation to run below.
    init_global = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    config1 = tf.ConfigProto()
    config1.gpu_options.allow_growth = True
    sess = tf.Session(config=config1)
    sess.run(init_global)
    step = 0

    #summary values
    if config['is_board']:
      TB = CTensorBoard()
      summary_writer = tf.summary.FileWriter(config['tb_logs']+name_model, graph=sess.graph)
      step_summary = 0

    #metrics for each task
    output_matrix_S = np.zeros([config['num_classes'], 3])
    
    if config['load_param']:
      ckpt = tf.train.get_checkpoint_state(os.path.dirname(os.path.join(config['path_param'],'checkpoint')))
      if ckpt: 
        import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
        variable_to_restore = get_variables_to_restore(import_variables)
        print('total_variables_loaded:', len(import_variables))
        saver = tf.train.Saver(variable_to_restore)
        saver.restore(sess, ckpt.model_checkpoint_path)
        step = sess.run(tf.assign(global_step, step))
        step = int( str(ckpt.model_checkpoint_path).split('model.ckpt-')[-1] )+1
        print('------ Model Loaded from ' + ckpt.model_checkpoint_path + ' -------')
      else:
        if 'intialize' in config:
          reader = tf.train.NewCheckpointReader(config['intialize'])
          var_str = reader.debug_string().decode('utf-8')
          name_var = re.findall('[A-Za-z0-9/:_]+ ', var_str)
          import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
          initialize_variables = {} 
          for var in import_variables: 
            if var.name+' ' in  name_var:
              initialize_variables[var.name] = var

          saver = tf.train.Saver(initialize_variables)
          saver.restore(save_path=config['intialize'], sess=sess)
          print('-------- Pretrained Intialization ---------')
        saver = tf.train.Saver()

    if config['dataset'] == 'cityscape_11':
      num_classes = config['num_classes']  # 11+1
      energy_level = 6 # 5+1 
      # /work/Cityscapes_aug3/train   17850 # 17850 - (17850 % config['batch_size']*len(devices))
      # /work/Cityscapes_aug3/val     500
      # /work/Cityscapes_aug3/test    1525
      dataset = CITYSCAPES(input_paste="/drw/Cityscapes_aug3", \
                    num_classes=num_classes, energy_level=energy_level-1, use_threads=12*4, num_data_load=\
                    17850 - (17850 % (config['batch_size']*len(devices)) ) )#17850)#14000)
                    #150 - (150 % (config['batch_size']*len(devices)) ) )#17850)#14000)
      dataset.load_data(size=(384,768))
    elif config['dataset'] == 'synthia':
      num_classes = config['num_classes']  # 11+1
      energy_level = 6 # 5+1
      #input_paste="/datasets/synthia/", 9400 (7k train) (2.4k test)
      #input_paste="/datasets/synthia/Rand/", 7000
      dataset = SYNTHIA(input_paste="/drw/SYNTHIA/Rand", num_classes=num_classes, \
              energy_level=energy_level-1, use_threads=12*4, regression=False, num_data_load=\
              7000 - (7000 % (config['batch_size']*len(devices)) ) )
              #150 - (150 % (config['batch_size']*len(devices)) ) )
      dataset.load_data(size=(384,768)) 
    else:
      sys.exit('dataset undefined, [cityscape_11, synthia]')

    list_l = [];
    list_l_S_cross = []
    list_l_S_iou = []

    epochs = config['epochs']
    bt_sz = config['batch_size']*len(devices)
    for ep in range(epochs):
      print("\n########## epoch " + str(ep+1) + " --- " + config['dataset'] + " --- " + name_model + " ##########")
      total_batch = int(dataset.get_size_dataset('train')/bt_sz)
      for i in range(total_batch):
        batch_img, batch_gt_S, batch_gt_E, batch_gt_C, batch_gt_D, \
            batch_nameimg = dataset.next_batch(state='train', batch_size=bt_sz)
        if len(batch_img) == 0:
          continue

        batch_gt_S_ = np.expand_dims(batch_gt_S, axis=-1)
        #batch_gt_E_ = np.expand_dims(batch_gt_E, axis=-1)
        #batch_gt_C_ = np.expand_dims(batch_gt_C, axis=-1)
        #batch_gt_D_ = np.expand_dims(batch_gt_D, axis=-1)

        # get prob. gmm pi
        ppi_arr = np.zeros((batch_img.shape[0], int(config['num_classes'])))
        for j in range(batch_img.shape[0]):
          pp_ = np.unique(batch_gt_S_[j]).astype(np.uint8)
          ppi_arr[j][pp_] = 1.0

        feed_dict = {images_pl:batch_img, labels_pi_pl:ppi_arr, labels_S_pl1:batch_gt_S_, batch_size_pl:bt_sz}

        fetches = [ train_op, tower_losses, tower_loss_S_cross, tower_loss_S_iou, logits_S ] 
        step += 1

        res = sess.run(fetches, feed_dict)
        _, loss_batch, l_S_cross, l_S_iou, prob_S = res[0], res[1], res[2], res[3], res[4]

        list_l.append(loss_batch)
        list_l_S_cross.append(l_S_cross)
        list_l_S_iou.append(l_S_iou)
        prediction_S = np.argmax(prob_S, 3)
        gt_S = batch_gt_S
        prediction_S[gt_S == config['unlabel']] = config['unlabel']
        output_matrix_S = compute_output_matrix(gt_S, prediction_S, output_matrix_S)
        
        if (step + 1) % config['save_step'] == 0:
          import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
          saver = tf.train.Saver(import_variables)
          saver.save(sess, os.path.join(config['checkpoint']+'/'+name_model, 'model.ckpt'), step+1)

        if (step + 1) % config['skip_step'] == 0:
          total_loss = np.mean(list_l)
          total_loss_S_cross = np.mean(list_l_S_cross)
          total_loss_S_iou = np.mean(list_l_S_iou)
          total_S_iou = compute_iou(output_matrix_S, -1)

          print("Step {} lr={:6.6} loss={:6.4f} iou={:6.4f} l_S={:6.4f} l_S_iou={:6.4f}".format(step, lr.eval(session=sess), total_loss, \
                      total_S_iou, total_loss_S_cross, total_loss_S_iou), end = '')
          print()
          # ------------------- summary  ------------------- 
          if config['is_board']:
            req = [ TB.loss_sc, TB.loss_S_cross_sc, TB.loss_S_iou_sc, TB.iou_S ] 
            
            feed_summary = {TB.loss_ph:total_loss, TB.loss_S_cross_ph:total_loss_S_cross, \
              TB.loss_S_iou_ph:total_loss_S_iou, TB.iou_S_ph:total_S_iou}

            res_summ = sess.run(req, feed_dict=feed_summary)
            _l, _l_S_cross, _l_S_iou, _iou_S = res_summ[0], res_summ[1], res_summ[2], res_summ[3]

            summary_writer.add_summary(_l, step_summary)
            summary_writer.add_summary(_l_S_cross, step_summary)
            summary_writer.add_summary(_l_S_iou, step_summary)
            summary_writer.add_summary(_iou_S, step_summary)
            step_summary += 1
          
      list_l.clear()
      output_matrix_ss.fill(0)
    
    saver.save(sess, os.path.join(config['checkpoint']+'/'+name_model, 'model.ckpt'), step-1)
    print('training_completed')
    sess.close()

def load_dataset(config):
    #os.environ['CUDA_VISIBLE_DEVICES'] = config['gpu_id']
    data_bt, label_bt, bound_bt, contourclass_bt, energy_bt, iterator = get_train_data(config)
    with tf.Session() as sess:
      image_, label_, bound_, contourclass_, energy_ = sess.run([data_bt, label_bt, bound_bt, contourclass_bt, energy_bt])
      print(image_.shape, label_.shape, bound_.shape, contourclass_.shape, energy_.shape)

def main():
    args = PARSER.parse_args()
    if args.config:
        file_address = open(args.config)
        config = yaml.load(file_address)
    else:
        print('--config config_file_address missing')
    #load_dataset(config)
    train_func(config)

if __name__ == '__main__':
    main()  
