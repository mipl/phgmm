import numpy as np
import tensorflow as tf
#https://www.tensorflow.org/tutorials/load_data/tfrecord#reading_a_tfrecord_file

def get_train_batch(config, reg, bt_sz):
    filenames = [config['train_data']]
    dataset = tf.data.TFRecordDataset(filenames)
    # preprocessing
    dataset = dataset.map(lambda x: parser(x, config['num_classes'], config['energy_level'], reg))
    #dataset = dataset.map(_parse_image_function)
    dataset = dataset.shuffle(buffer_size=1000)
    dataset = dataset.batch(bt_sz, drop_remainder=True)
    dataset = dataset.repeat(1)
    dataset = dataset.prefetch(1000)#(config['batch_size']) #(1)
    #iterator = dataset.make_initializable_iterator()
    #iterator = dataset.make_one_shot_iterator()
    return dataset#iterator

def get_train_data(config, reg, bt_sz):
    iterator = get_train_batch(config, reg, bt_sz)
    dataA, label, bound, contourclass, energy = iterator.get_next()
    return dataA, label, bound, contourclass, energy, iterator

def get_test_data(config):
    iterator = get_test_batch(config)
    dataA, label, bound, contourclass, energy = iterator.get_next()
    return dataA, label, bound, contourclass, energy, iterator

def get_test_batch(config):
    filenames = [config['test_data']]
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(lambda x: parser(x, config['num_classes'], config['energy_level']))
    dataset = dataset.batch(config['batch_size'])
    iterator = dataset.make_initializable_iterator()
    return iterator

def compute_output_matrix(label_max, pred_max, output_matrix):
    # Input:
    # label_max shape(B,H,W): np.argmax(one_hot_encoded_label,3)
    # pred_max shape(B,H,W): np.argmax(softmax,3)
    # output_matrix shape(NUM_CLASSES,3): if func is called first time an array of 
    #                                     zeros.
    # Output:
    # output_matrix shape(NUM_CLASSES,3): columns with total count of true positives,
    #                                     false positives and false negatives.
    for i in range(output_matrix.shape[0]):
        temp = pred_max == i
        temp_l = label_max == i
        tp = np.logical_and(temp, temp_l)
        temp[temp_l] = True
        fp = np.logical_xor(temp, temp_l)
        temp = pred_max == i
        temp[fp] = False
        fn = np.logical_xor(temp, temp_l)
        output_matrix[i, 0] += np.sum(tp)
        output_matrix[i, 1] += np.sum(fp)
        output_matrix[i, 2] += np.sum(fn)

    return output_matrix

def compute_iou(output_matrix, unlabel=-1, show_class=False): #unlabel is the background no segmentation, [0, num_class-1]
    # Input:
    # output_matrix shape(NUM_CLASSES,3): columns with total count of true positives,
    #                                     false positives and false negatives.
    # Output:
    # IoU in percent form (doesn't count label id 0 contribution as it is assumed to be void) 
    #'''
    #output_matrix2 = output_matrix.copy()
    if unlabel != -1:
        output_matrix = np.delete(output_matrix, unlabel, 0)
    #output_matrix = output_matrix[0:11,:]
    #output_matrix = output_matrix[1:,:]
    #'''
    list_iou = []
    for i in range(output_matrix.shape[0]):
        list_iou.append( np.sum(output_matrix[i, 0]/(np.sum(output_matrix[i, :], 0).astype(np.float32)+1e-10))*100 )
    miou = np.nanmean(list_iou)
    if show_class:
        print(list_iou, " mean iou: ", miou)
    return miou
    #'''
    #return np.sum(output_matrix2[1:, 0]/(np.sum(output_matrix2[1:, :], 1).astype(np.float32)+1e-10))/(output_matrix2.shape[0]-1)*100

def parser(proto_data, num_classes, level_energy, reg):

    features = {'height':tf.FixedLenFeature((), tf.int64, default_value=0),
                'width':tf.FixedLenFeature((), tf.int64, default_value=0),
                'modality1':tf.FixedLenFeature((), tf.string, default_value=""),
                'label':tf.FixedLenFeature((), tf.string, default_value=""),
                'bound':tf.FixedLenFeature((), tf.string, default_value=""),
                'contourclass':tf.FixedLenFeature((), tf.string, default_value=""),
                'energy':tf.FixedLenFeature((), tf.string, default_value="")
               }
    parsed_features = tf.parse_single_example(proto_data, features)
    modality1 = tf.decode_raw(parsed_features['modality1'], tf.uint8)
    label = tf.decode_raw(parsed_features['label'], tf.uint8)
    bound = tf.decode_raw(parsed_features['bound'], tf.uint8)
    contourclass = tf.decode_raw(parsed_features['contourclass'], tf.uint8)
    energy = tf.decode_raw(parsed_features['energy'], tf.uint8)
    #if reg:
     #   energy = tf.decode_raw(parsed_features['energy'], tf.float32)
    #else:
     #   energy = tf.decode_raw(parsed_features['energy'], tf.uint8)
    

    height = tf.cast(parsed_features['height'], tf.int32)
    width = tf.cast(parsed_features['width'], tf.int32)

    modality1 = tf.reshape(modality1, [height, width, 3])
    modality1= tf.cast(modality1, tf.float32)

    label = tf.reshape(label, [height, width, 1])
    label = tf.one_hot(label, num_classes)
    label = tf.squeeze(label, axis=2)
    label = tf.cast(label, tf.int32)
    
    bound = tf.reshape(bound, [height, width, 1])
    bound = tf.cast(bound, tf.int32)

    contourclass = tf.reshape(contourclass, [height, width, 1])
    contourclass = tf.one_hot(contourclass, num_classes)
    contourclass = tf.squeeze(contourclass, axis=2)
    contourclass = tf.cast(contourclass, tf.int32)
    '''
    if reg:
        energy = tf.reshape(energy, [height, width, 1])
        energy = tf.cast(energy, tf.float32)
    else:
        energy = tf.reshape(energy, [height, width, 1])
        energy = tf.one_hot(energy, level_energy)
        energy = tf.squeeze(energy, axis=2)
        energy = tf.cast(energy, tf.int32)
    '''
    energy = tf.reshape(energy, [height, width, 1])
    energy = tf.one_hot(energy, level_energy)
    energy = tf.squeeze(energy, axis=2)
    energy = tf.cast(energy, tf.int32)
  
    #assert(False), [modality1.get_shape().as_list(), label.get_shape().as_list(), bound.get_shape().as_list(),\
     #   contourclass.get_shape().as_list(), energy.get_shape().as_list()]
    
    #return tf.cast(modality1, tf.float32), tf.cast(label, tf.int32), tf.cast(bound, tf.int32), \
    #      tf.cast(contourclass, tf.int32), tf.cast(energy, tf.int32)

    return modality1, label, bound, contourclass, energy
