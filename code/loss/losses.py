import tensorflow as tf
import numpy as np
import random
import time
import math

class LossFunc:

  def fixprob(self, att):
    """make the tensor in (0,1), normalization"""
    att = tf.nn.sigmoid(att)
    att = att + 1e-9
    _sum = tf.reduce_sum(att, reduction_indices=1, keep_dims=True)
    att = att / _sum
    att = tf.clip_by_value(att, 1e-9, 1.0, name=None)
    #assert(False), att.get_shape().as_list()
    return att

  def _kl_divergence_gaussians(self, mean_pri, log_var_pri, mean_pos, log_var_pos):
    '''https://tiao.io/post/density-ratio-estimation-for-kl-divergence-minimization-between-implicit-distributions/'''
    #log_var_pri = tf.log(tf.clip_by_value(log_var_pri,1e-10,1.0))
    #log_var_pos = tf.log(tf.clip_by_value(log_var_pos,1e-10,1.0))
    eps = 1e-10
    #log_var_pri = self.fixprob(log_var_pri)
    #log_var_pos = self.fixprob(log_var_pos)
    r = mean_pri - mean_pos
    kl_gauss = (tf.log(log_var_pos+eps) - tf.log(log_var_pri+eps) - .5 * (1. - (log_var_pri**2 + r**2) / log_var_pos**2))
    return tf.reduce_mean(kl_gauss)

  def kl_gaussian(self, mean, var, average=False):
    """KL Divergence between the posterior and a prior gaussian distribution (N(0,1))
       loss = (1/n) * -0.5 * Σ(1 + log(σ^2) - σ^2 - μ^2)
    Args:
        mean: (array) corresponding array containing the mean of our inference model
        logVar: (array) corresponding array containing the log(variance) of our inference model
        average: (bool) whether to average the result to obtain a value

    Returns:
        output: (array/float) depending on average parameters the result will be the mean
                              of all the sample losses or an array with the losses per sample
    """
    eps = 1e-10
    logVar = tf.log(tf.clip_by_value(var,1e-10,1.0))
    #var = self.fixprob(var)
    #logVar = tf.log(var + eps)
    loss = -0.5 * tf.reduce_sum(1 + logVar - tf.exp(logVar) - tf.square(mean + eps), 1 ) 
    #loss = -0.5 * tf.reduce_sum(1. + logVar - mean**2 - tf.exp(logVar))
    if average:
      return tf.reduce_mean(loss)
    else:
      return tf.reduce_sum(loss)

  def loss_cross(self, softmax, label, weights):
    loss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label*tf.log(softmax+1e-10), weights), axis=[3]))
    return loss

  def Dice_square_loss(self, logits, labels, multilabel=True):
    '''Approximate Dice coef. loss from
    https://arxiv.org/abs/1606.04797 (V-net: Fully Convolutional Neural Networks for Volumetric Medical Image Segmentation )
    '''
    labels = tf.reshape(labels, [-1])
    logits = tf.reshape(logits, [-1])
    labels_f32 = tf.cast(labels, tf.float32)
    inter = tf.reduce_sum(tf.multiply(logits, labels_f32, name='mul_b_sum'))
    denominator = tf.add(tf.reduce_sum(tf.square(logits)), tf.reduce_sum(tf.square(labels_f32)))
    if multilabel:
      total_dice_loss = tf.div(2.0*inter, denominator+1e-10)
    else: #binary
      total_dice_loss = tf.subtract(tf.constant(1.0, dtype=tf.float32), tf.div(2.0*inter, denominator+1e-10))
    return tf.abs(total_dice_loss)

  def Dice_square_loss_multilabel(self, logits, labels, num_classes):
    loss_dice_sq = 0.0
    for c in range(num_classes):
      loss_dice_sq += self.Dice_square_loss(logits[:,:,:,c], labels[:,:,:,c])
    mean_loss_dice_sq = tf.subtract(tf.constant(1.0, dtype=tf.float32), tf.div(loss_dice_sq, num_classes))
    return tf.abs(mean_loss_dice_sq)

  def IoU_loss(self, logits, labels, multilabel=True):
    '''Approximate IoU coef. loss from
    http://www.cs.umanitoba.ca/~ywang/papers/isvc16.pdf'''
    #labels = tf.cast( labels, tf.float32)
    labels = tf.reshape(labels, [-1])

    # Approximate IoU loss from
    # http://www.cs.umanitoba.ca/~ywang/papers/isvc16.pdf
    logits = tf.reshape(logits, [-1])

    # binarize the network output
    prediction = tf.cast(tf.greater_equal(logits, 0.5), tf.int32)

    #trn_labels = tf.reshape(trn_labels_batch, [-1])
    labels_f32 = tf.cast(labels, tf.float32)
    inter = tf.reduce_sum(tf.multiply(logits, labels_f32, name='mul_b_sum'))
    union = tf.reduce_sum(tf.subtract(tf.add(logits, labels_f32), tf.multiply(logits, labels_f32)))

    if multilabel:
      total_iou_loss = tf.div(inter, union+1e-10)#tf.subtract(tf.constant(1.0, dtype=tf.float32), tf.div(inter, union+1e-10))
    else: #binary
      total_iou_loss = tf.subtract(tf.constant(1.0, dtype=tf.float32), tf.div(inter, union+1e-10))
    return tf.abs(total_iou_loss)

  def IoU_loss_multilabel(self, logits, labels, num_classes):
    loss_iou = 0.0
    for c in range(num_classes):
      loss_iou += self.IoU_loss(logits[:,:,:,c], labels[:,:,:,c])
    mean_loss_iou = tf.subtract(tf.constant(1.0, dtype=tf.float32), tf.div(loss_iou, num_classes))
    return tf.abs(mean_loss_iou)

  def IoU_loss_another(self, logits, labels):
    '''
    now, logits is output with shape [batch_size x img h x img w x 1]
    and represents probability of class 1
    https://angusg.com/writing/2016/12/28/optimizing-iou-semantic-segmentation.html
    https://arxiv.org/pdf/1608.01471.pdf
    http://www.cs.umanitoba.ca/~ywang/papers/isvc16.pdf
    https://www.cs.toronto.edu/~urtasun/publications/mattyus_etal_iccv17.pdf
    '''
    labels = tf.cast( labels, tf.float32)
    logits=tf.reshape(logits, [-1])
    trn_labels=tf.reshape(labels, [-1])
    inter=tf.reduce_sum(tf.multiply(logits,trn_labels))
    union=tf.reduce_sum(tf.subtract(tf.add(logits,trn_labels),tf.multiply(logits,trn_labels)))
    loss=tf.subtract(tf.constant(1.0, dtype=tf.float32),tf.div(inter,union))
    return tf.abs(loss)

  def weighted_loss(self, logits, labels, number_class, frequency_w):
    """
    https://datascience.stackexchange.com/questions/15181/multi-task-learning-finding-a-loss-function-that-ignores-certain-samples/15182#15182
    The reference paper is : https://arxiv.org/pdf/1411.4734.pdf
    Median Frequency Balancing: alpha_c = median_freq/freq(c).
    median_freq is the median of these frequencies
    freq(c) is the number of pixels of class c divided by the total number of pixels in images where c is present
    we weight each pixels by alpha_c
    Inputs:
    logits is the output from the inference, which is the output of the decoder layers without softmax.
    labels: true label information
    number_class: In the CamVid data set, it's 7 classes, or 8, because class 7 seems to be background?
    frequency: is the frequency of each class
    Outputs:
    Loss
    Accuracy
    """
    labels = tf.to_int64(labels)
    label_flatten = tf.reshape(labels, [-1])
    label_onehot = tf.one_hot(label_flatten, depth=number_class)
    logits_reshape = tf.reshape(logits, [-1, number_class])
    cross_entropy = tf.nn.weighted_cross_entropy_with_logits(targets=label_onehot,logits=logits_reshape,pos_weight=frequency_w)
    cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
    return cross_entropy_mean

  def weighted_loss_manual(self, logits, labels, num_classes, head=None):
    """median-frequency re-weighting """
    with tf.name_scope('loss'):
      labels = tf.to_int64(labels)
      logits = tf.reshape(logits, (-1, num_classes))
      epsilon = tf.constant(value=1e-10)
      logits = logits + epsilon
      # consturct one-hot label array
      label_flat = tf.reshape(labels, (-1, 1))
      # should be [batch ,num_classes]
      labels = tf.reshape(tf.one_hot(label_flat, depth=num_classes), (-1, num_classes))
      softmax = tf.nn.softmax(logits)
      cross_entropy = -tf.reduce_sum(tf.multiply(labels * tf.log(softmax + epsilon), head), axis=[1])
      loss = tf.reduce_mean(cross_entropy, name='cross_entropy') #cross entropy mean
    return loss


  def add_cross_entropy(self, labels, logits, batch_size):
    """Compute average cross entropy and add to loss collection.
    https://github.com/lisjin/dcan-tensorflow/blob/master/tf-dcan/bbbc006_train.py
    https://arxiv.org/pdf/1604.02677.pdf
    Args:
        labels: Single dimension labels from distorted_inputs() or inputs().
        logits: Output map from inference().
    """
    #with tf.variable_scope('{}_cross_entropy'.format(pref)) as scope:
    labels_ = labels
    labels = tf.to_int64(labels)
    #class_prop = self.objdet_boundary_weight[1]

    #-------------------------------------------
    count_neg = tf.reduce_sum(1.0 - labels_) # the number of 0 in y
    count_pos = tf.reduce_sum(labels_) # the number of 1 in y (less than count_neg)
    class_prop = count_pos/(count_pos + count_neg)
    #-------------------------------------------

    weight_per_label = tf.scalar_mul(class_prop, tf.cast(tf.equal(labels, 0),tf.float32)) + \
                       tf.scalar_mul(1.0 - class_prop, tf.cast(tf.equal(labels, 1),tf.float32))
    #logits = tf.maximum(logits, 1e-15) #avoid nan because log(0) -> log(logist)
    #logits = tf.minimum(logits, 1.0-(1e-7)) #avoid nan because log(1-1) -> log(1-logist)
    logits = tf.clip_by_value(logits,1e-15,1.0-(1e-7))
    cross_entropy = -tf.reduce_sum (labels_*tf.log(logits) + (1.0-labels_)*tf.log(1.0-logits))
    #cross_entropy = tf.losses.sparse_softmax_cross_entropy(labels=tf.squeeze(labels, squeeze_dims=[3]),logits=logits)
    cross_entropy_weighted = tf.multiply(weight_per_label, cross_entropy,name='boundary_cross_entropy')
    cross_entropy_mean = tf.reduce_mean(cross_entropy_weighted)
    return tf.divide( cross_entropy_mean, tf.cast(batch_size, tf.float32)*tf.cast(tf.shape(labels)[1], tf.float32)*10.0 )#self.input_h*self.input_w )

  def class_balanced_sigmoid_cross_entropy(self, logits, label, name='cross_entropy_loss'):
    """
    The class-balanced cross entropy loss,
    as in `Holistically-Nested Edge Detection
    <http://arxiv.org/abs/1504.06375>`_.
    This is more numerically stable than class_balanced_cross_entropy
    :param logits: size: the logits.
    :param label: size: the ground truth in {0,1}, of the same shape as logits.
    :returns: a scalar. class-balanced cross entropy loss
    """
    y = tf.cast(label, tf.float32)

    count_neg = tf.reduce_sum(1.0 - y) # the number of 0 in y
    count_pos = tf.reduce_sum(y) # the number of 1 in y (less than count_neg)
    beta = count_neg / (count_neg + count_pos)

    pos_weight = beta / (1.0 - beta)
    cost = tf.nn.weighted_cross_entropy_with_logits(logits, y, pos_weight)
    # cost = -tf.reduce_mean(cost * (1 - beta), name=name)
    cost = tf.reduce_mean(cost * (1.0 - beta), name=name)
    return cost

  def HED_loss(self, logits, labels, name='HED_loss'):
    '''
    original from 'Holistically-Nested Edge Detection (CVPR 15)'
    https://github.com/conan7882/richer-conv-feature-edge/blob/master/lib/models/loss.py
    '''
    with tf.name_scope(name) as scope:
      logits = tf.cast(logits, tf.float32)
      labels = tf.cast(labels, tf.float32)

      num_pos = tf.reduce_sum(labels)
      num_neg = tf.reduce_sum(1.0 - labels)

      beta = num_neg / (num_neg + num_pos)
      pos_weight = beta / (1 - beta)
      #check_weight = tf.identity(beta, name = 'check')

      cost = tf.nn.weighted_cross_entropy_with_logits(targets=labels,
                                                      logits=logits,
                                                      pos_weight=pos_weight)
      # print(cost)
      # t = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=logits))
      loss = tf.reduce_mean((1 - beta) * cost)
      return tf.where(tf.equal(beta, 1.0), 0.0, loss)


  def normal_loss(self, logits, labels, number_class):
    """
    Using tf.nn.sparse_softmax_cross_entropy_with_logits assume that each pixel have and only have one specific
    label, instead of having a probability belongs to labels. Also assume that logits is not softmax.
    """
    label_flatten = tf.to_int64(tf.reshape(labels, [-1]))
    logits_reshape = tf.reshape(logits, [-1, number_class])
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=label_flatten,\
        logits=logits_reshape,name='normal_cross_entropy')
    #solve nan problem https://github.com/tensorflow/tensorflow/issues/4443
    #https://github.com/tensorflow/tensorflow/issues/10268
    #--cross_entropy_ = tf.where( tf.is_nan(cross_entropy),tf.zeros(tf.shape(cross_entropy)),cross_entropy)
    #--cross_entropy_mean = tf.reduce_mean(cross_entropy_, name='cross_entropy')
    #weight loss
    cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
    return cross_entropy_mean
    #cross_entropy_mean = -tf.reduce_sum(label_flatten*tf.log(tf.nn.softmax(logits_reshape) + 1e-10))
    #return cross_entropy_mean

