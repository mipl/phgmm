import matplotlib.pyplot as plt
plt.rcParams.update({'figure.max_open_warning': 0})
plt.switch_backend('agg')
from PIL import Image, ImageFilter
import numpy as np

def normalize(x):
  #normalized_x = x / np.linalg.norm(x) #slow operation
  #normalized_x = x / np.sqrt( np.sum( x**2 ) )
  if np.max(x) != np.min(x):
    normalized_x = ( x - np.min(x) ) / ( np.max(x) - np.min(x) ) #[0,1]
  else:
    normalized_x = x
  return normalized_x

def writeFloatImage(image, filename, dpi_=300, cmap_='plasma'):
    if len(image.shape) == 3:
      h, w, c = image.shape
    else:
      h, w = image.shape
    image = image.reshape((h,w))
    figsize = w/float(dpi_), h/float(dpi_)
    fig = plt.figure(figsize=figsize)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('off')
    #cmap styles https://matplotlib.org/examples/color/colormaps_reference.html
    ax.imshow(image, cmap=cmap_) #'viridis', 'plasma', 'gray', 'afmhot',
    fig.savefig(filename, pad_inches=0, dpi=dpi_)

def fig2data ( fig ):
    """
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    # draw the renderer
    fig.canvas.draw ( )
 
    # Get the RGBA buffer from the figure
    w,h = fig.canvas.get_width_height()
    buf = np.fromstring ( fig.canvas.tostring_argb(), dtype=np.uint8 )
    buf.shape = ( w, h,4 )
 
    # canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
    buf = np.roll ( buf, 3, axis = 2 )
    return buf

def fig2img ( fig ):
    """
    @brief Convert a Matplotlib figure to a PIL Image in RGBA format and return it
    @param fig a matplotlib figure
    @return a Python Imaging Library ( PIL ) image
    """
    # put the figure pixmap into a numpy array
    buf = fig2data ( fig )
    w, h, d = buf.shape
    return Image.frombytes( "RGBA", ( w ,h ), buf.tostring( ) ) #(h,w,4)

#https://github.com/insikk/Grad-CAM-tensorflow

def visualize(image, conv_output, conv_grad, gb_viz, filename):
    output = conv_output           # [h,w,channel]
    #grads_val = normalize(conv_grad)          # [h,w,channel]
    grads_val = np.zeros_like(conv_grad)
    for i in range(conv_grad.shape[2]):
      grads_val[:,:,i] = normalize(conv_grad[:,:,i])
    #grads_val = normalize(zeros_like)
    #print("image shape: ", image.shape)
    #print("grads_val shape:", grads_val.shape)
    #print("gb_viz shape:", gb_viz.shape)

    if len(image.shape) == 3:
      img_h, img_w, img_c = image.shape
    else:
      img_h, img_w = image.shape
    # cmap styles https://matplotlib.org/examples/color/colormaps_reference.html
    cmap_ = 'jet' #'viridis', 'plasma', 'gray', 'afmhot', 'jet'
    dpi_ = 100

    weights = np.mean(grads_val, axis = (0, 1)) # alpha_k, [32]
    cam = np.zeros(output.shape[0 : 2], dtype = np.float32)	# [63,125]

    #weights = weights + np.abs(np.min(weights))
    #weights /= (np.max(weights)+1e-10)
    #print("weights.unique: ", np.unique(weights))
    #print("output.unique: ", np.unique(output))
    
    #print("--------> ", weights.shape, weights)
    # Taking a weighted average
    for i, w in enumerate(weights):
        cam += w * output[:, :, i]

    # Passing through ReLU
    cam = np.maximum(cam, 0)
    #cam = cam / (np.max(cam)+1e-10) # scale 0 to 1.0
    cam = normalize(cam)
    cam = Image.fromarray(cam)
    cam = cam.resize(size=(img_w,img_h), resample=Image.NEAREST)
    cam_heatmap = np.asarray(cam)
    
    #figsize = img_w, img_h
    figsize = img_w/float(dpi_), img_h/float(dpi_)
    fig = plt.figure(figsize=figsize)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('off')
    ax.imshow(cam_heatmap, cmap=cmap_)
    #fig.savefig(filename, pad_inches=0, dpi=dpi_)
    camrgba = fig2img(fig) #(500, 1000, 4)
    cam_grad = np.array(camrgba)[:,:,0:3] 
    #cam_grad = np.array(Image.fromarray(cam_grad).filter(ImageFilter.GaussianBlur(radius = 3)))
    #imrgba.save(filename)

    #im = Image.fromarray(np.uint8(image))
    #im.save(filename2)
    #cam_grad[ cam_grad <= 0.6 ] = 0.0
    cam2 = np.float32(cam_grad)*0.5 + np.float32(image)*1.0
    cam2 = 255 * cam2 / (np.max(cam2)+1e-10)

    im2 = Image.fromarray(np.uint8(cam2))
    im2.save(filename)

    name_file_grad = filename.replace('/grad_cam/', '/grad_cam/grad/')
    name_file_grad = name_file_grad.replace('.png', '.npy')
    np.save(name_file_grad, cam_heatmap)

    #------------- regions no activate --------
    #cam_heatmap_invert = np.max(cam_heatmap) - cam_heatmap
    #cam_heatmap_invert = normalize(cam_heatmap_invert)

    #name_cam3 = filename.replace('/grad_cam/', '/grad_cam/invert/')
    #writeFloatImage(cam_heatmap_invert, name_cam3, cmap_=cmap_)

    #img = image.astype(float)
    #img -= np.min(img)
    #img /= img.max()    
    #cam = np.expand_dims(np.asarray(cam), axis=-1)
    #cam = np.float32(cam) + np.float32(img)
    #cam = 255 * cam / (np.max(cam)+1e-10)
    #cam = np.uint8(cam)
    #figsize = img_w/float(dpi_), img_h/float(dpi_)
    #fig = plt.figure(figsize=figsize)
    #ax = fig.add_axes([0, 0, 1, 1])
    #ax.axis('off')
    #ax.imshow(cam, cmap=cmap_)
    #fig.savefig(filename, pad_inches=0, dpi=dpi_)
              
    '''
    fig = plt.figure()    
    ax = fig.add_subplot(111)
    imgplot = plt.imshow(img)
    ax.set_title('Input Image')
    
    fig = plt.figure(figsize=(12, 16))    
    ax = fig.add_subplot(131)
    imgplot = plt.imshow(cam_heatmap)
    ax.set_title('Grad-CAM')    
    
    gb_viz = np.dstack((
            gb_viz[:, :, 0],
            gb_viz[:, :, 1],
            gb_viz[:, :, 2],
        ))       
    gb_viz -= np.min(gb_viz)
    gb_viz /= gb_viz.max()

    ax = fig.add_subplot(132)
    imgplot = plt.imshow(gb_viz)
    ax.set_title('guided backpropagation')
    

    gd_gb = np.dstack((
            gb_viz[:, :, 0] * cam,
            gb_viz[:, :, 1] * cam,
            gb_viz[:, :, 2] * cam,
        ))            
    ax = fig.add_subplot(133)
    imgplot = plt.imshow(gd_gb)
    ax.set_title('guided Grad-CAM')

    plt.show()
    '''
