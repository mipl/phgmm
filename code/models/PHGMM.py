import tensorflow as tf
from models import network_base
from loss.losses import LossFunc

class PHGMM(network_base.Network):
    def __init__(self, num_classes=12, k=12, learning_rate=0.001, float_type=tf.float32, weight_decay=0.0005,
                 decay_steps=30000, power=0.9, training=True, ignore_label=True, global_step=0,
                 has_aux_loss=True):
        
        super(PHGMM, self).__init__()
        self.num_classes = num_classes
        self.K = k
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.initializer = 'he'
        self.has_aux_loss = has_aux_loss
        self.float_type = float_type
        self.power = power
        self.decay_steps = decay_steps
        self.training = training
        self.bn_decay_ = 0.99
        self.residual_units = [3, 4, 23, 3]
        self.global_step = global_step
      
        self.list_index_task_curr = []
        self.list_index_task_curr.append(0)

        self.loss_func = LossFunc()

        if self.training:
            self.keep_prob = 0.3
        else:
            self.keep_prob = 1.0
        if ignore_label:
            self.weights = tf.ones(self.num_classes-1)
            self.weights = tf.concat((tf.zeros(1), self.weights), 0)
        else:
            self.weights = tf.ones(self.num_classes)

    def postNet(self, data, label_S, name):   
        with tf.variable_scope(name):
          input_shape = data.get_shape()
          data_concat = tf.concat( [data, label_S], -1 )
          # ----------------------------- ResNet 101 ------------------------------
          # Encoder
          with tf.variable_scope('conv0'):
            data_after_bn = self.batch_norm(data_concat)
          #max_pool_out = self.conv_batchN_relu(data_after_bn, 7, 2, 64, name='conv1')
          max_pool_out = self.conv_batchN_relu(data_after_bn, 7, 1, 64, name='conv1')
          #max_pool_out = self.pool(conv_7x7_out, 2, 2)

          ## enc_block1
          m_b1_out = self.residual_block(max_pool_out, [64,64,256], 1,  True, 1, 1)
          shape_b1 = m_b1_out.get_shape()
          for unit_index in range(1, self.residual_units[0]):
            m_b1_out = self.residual_block(m_b1_out, [64,64,256], 1, False, 1, unit_index+1)
          enc_b1 = m_b1_out
          with tf.variable_scope('block1/unit_%d/bottleneck_v2/conv3'%(self.residual_units[0]+1)):
            m_b1_out = tf.nn.relu(self.batch_norm(m_b1_out))
          
          ## enc_block2
          m_b2_out = self.residual_block(m_b1_out, [128, 128, 512], 2, True, 2, 1)
          shape_b2 = m_b2_out.get_shape()
          for unit_index in range(1, self.residual_units[1]):
            m_b2_out = self.residual_block(m_b2_out, [128, 128, 512], 1, False, 2, unit_index+1)
          enc_b2 = m_b2_out
          with tf.variable_scope('block2/unit_%d/bottleneck_v2/conv3'%(self.residual_units[1]+1)):
            m_b2_out = tf.nn.relu(self.batch_norm(m_b2_out))

          ## enc_block3
          m_b3_out = self.residual_block(m_b2_out, [256, 256, 1024], 2, True, 3, 1, 2)
          shape_b3 = m_b3_out.get_shape()
          for unit_index in range(1, self.residual_units[2]):
            m_b3_out = self.residual_block(m_b3_out, [256, 256, 1024], 1, False, 3, unit_index+1, 2)
          enc_b3 = m_b3_out
          with tf.variable_scope('block3/unit_%d/bottleneck_v2/conv3'%(self.residual_units[2]+1)):
            m_b3_out = tf.nn.relu(self.batch_norm(m_b3_out))

          ## enc_block4
          m_b4_out = self.residual_block(m_b3_out, [512, 512, 2048], 2, True, 4, 1, 4)
          #shape_b4 = m_b4_out.get_shape()
          for unit_index in range(1, self.residual_units[3]):
            m_b4_out = self.residual_block(m_b4_out, [512, 512, 2048], 1, False, 4, unit_index+1, 4)
          with tf.variable_scope('block4/unit_%d/bottleneck_v2/conv3'%(self.residual_units[3]+1)):
            m_b4_out = tf.nn.relu(self.batch_norm(m_b4_out))

          # -------------------- So Now the encoder process has been Finished ---------------------#

          with tf.variable_scope('GMM'):
            c_0 = self.compress_feature(m_b4_out)
            c_1 = self.compress_feature(m_b3_out)
            c_2 = self.compress_feature(m_b2_out)
            c_3 = self.compress_feature(m_b1_out)
            c_merge = self.fc(inputs=tf.concat([c_0,c_1,c_2,c_3], -1), out_channels=256, name='merge')

            z_mu_k, z_var_k, z_k = [[None] * self.K for i in range(3)]
            for i in range(self.K):
              mu_z_k_, var_z_k_, z_ = self.dist_gaussian(c_merge, 256, 'gaussian_k'+str(i))
              z_mu_k[i] = mu_z_k_
              z_var_k[i] = var_z_k_
              z_k[i] = z_
        
            return z_mu_k, z_var_k
     
    def forward(self, data, batch_size):   
        input_shape = data.get_shape()
        # ----------------------------- ResNet 101 ------------------------------
        # Encoder
        with tf.variable_scope('conv0'):
          data_after_bn = self.batch_norm(data)
        #max_pool_out = self.conv_batchN_relu(data_after_bn, 7, 2, 64, name='conv1')
        max_pool_out = self.conv_batchN_relu(data_after_bn, 7, 1, 64, name='conv1')
        #max_pool_out = self.pool(conv_7x7_out, 2, 2)

        ## enc_block1
        m_b1_out = self.residual_block(max_pool_out, [64,64,256], 1,  True, 1, 1)
        shape_b1 = m_b1_out.get_shape()
        for unit_index in range(1, self.residual_units[0]):
          m_b1_out = self.residual_block(m_b1_out, [64,64,256], 1, False, 1, unit_index+1)
        enc_b1 = m_b1_out
        with tf.variable_scope('block1/unit_%d/bottleneck_v2/conv3'%(self.residual_units[0]+1)):
          m_b1_out = tf.nn.relu(self.batch_norm(m_b1_out))
        
        ## enc_block2
        m_b2_out = self.residual_block(m_b1_out, [128, 128, 512], 2, True, 2, 1)
        shape_b2 = m_b2_out.get_shape()
        for unit_index in range(1, self.residual_units[1]):
          m_b2_out = self.residual_block(m_b2_out, [128, 128, 512], 1, False, 2, unit_index+1)
        enc_b2 = m_b2_out
        with tf.variable_scope('block2/unit_%d/bottleneck_v2/conv3'%(self.residual_units[1]+1)):
          m_b2_out = tf.nn.relu(self.batch_norm(m_b2_out))

        ## enc_block3
        m_b3_out = self.residual_block(m_b2_out, [256, 256, 1024], 2, True, 3, 1, 2)
        shape_b3 = m_b3_out.get_shape()
        for unit_index in range(1, self.residual_units[2]):
          m_b3_out = self.residual_block(m_b3_out, [256, 256, 1024], 1, False, 3, unit_index+1, 2)
        enc_b3 = m_b3_out
        with tf.variable_scope('block3/unit_%d/bottleneck_v2/conv3'%(self.residual_units[2]+1)):
          m_b3_out = tf.nn.relu(self.batch_norm(m_b3_out))

        ## enc_block4
        m_b4_out = self.residual_block(m_b3_out, [512, 512, 2048], 2, True, 4, 1, 4)
        #shape_b4 = m_b4_out.get_shape()
        for unit_index in range(1, self.residual_units[3]):
          m_b4_out = self.residual_block(m_b4_out, [512, 512, 2048], 1, False, 4, unit_index+1, 4)
        with tf.variable_scope('block4/unit_%d/bottleneck_v2/conv3'%(self.residual_units[3]+1)):
          m_b4_out = tf.nn.relu(self.batch_norm(m_b4_out))
        
        #m_b4_out = self.conv_batchN_relu(m_b4_out, 3, 1, 512, 'conv_ls')
        conv4_clf = tf.nn.sigmoid(self.conv2d(m_b4_out, 1, 1, self.num_classes, 'clf4_S'))
        conv3_clf = tf.nn.sigmoid(self.conv2d(m_b3_out, 1, 1, self.num_classes, 'clf3_S'))
        conv2_clf = tf.nn.sigmoid(self.conv2d(m_b2_out, 1, 1, self.num_classes, 'clf2_S'))
        conv1_clf = tf.nn.sigmoid(self.conv2d(m_b1_out, 1, 1, self.num_classes, 'clf1_S'))

        # -------------------- So Now the encoder process has been Finished ---------------------#

        with tf.variable_scope('GMM'):
          c_0 = self.compress_feature(m_b4_out)
          c_1 = self.compress_feature(m_b3_out)
          c_2 = self.compress_feature(m_b2_out)
          c_3 = self.compress_feature(m_b1_out)
          c_merge = self.fc(inputs=tf.concat([c_0,c_1,c_2,c_3], -1), out_channels=256, name='merge')
          prob_gmm = tf.nn.sigmoid( self.fc(inputs=c_0, out_channels=self.num_classes, name='prob_gmm') )

          z_mu_k, z_var_k, z_k = [[None] * self.K for i in range(3)]
          for i in range(self.K):
            mu_z_k_, var_z_k_, z_ = self.dist_gaussian(c_merge, 256, 'gaussian_k'+str(i))
            z_mu_k[i] = mu_z_k_
            z_var_k[i] = var_z_k_
            z_k[i] = z_

          z_mu_k_pi = [None] * self.K
          for h in range(self.K):
            z_mu_k_pi[h] = tf.multiply(tf.expand_dims(prob_gmm[:,h],-1), z_mu_k[h])#z_mu_k[h]) z_k[h])

          sz_b4 = m_b4_out.get_shape().as_list()
          #prob_gmm
          merge_conv = self.convert_fc_conv(z=z_mu_k_pi, k=self.K, h=sz_b4[1], w=sz_b4[2], channels=5, name='merge_conv')
          conv_comb = self.conv_batchN_relu(merge_conv, 3, 1, 512, name='conv_comb') #5*self.K

        # ---------------------------------------- Gauss MTL ---------------------------------
        with tf.variable_scope('Gauss_MTL'):
          g_c_0 = self.compress_feature(m_b4_out)
          g_c_1 = self.compress_feature(m_b3_out)
          g_c_2 = self.compress_feature(m_b2_out)
          g_c_3 = self.compress_feature(m_b1_out)
        
          g_c_merge = self.fc(inputs=tf.concat([g_c_0,g_c_1,g_c_2,g_c_3], -1), out_channels=256, name='g_merge')

          g_z_mu_k, g_z_var_k, g_z_k = [[None] * 4 for i in range(3)]
          for task_idx in self.list_index_task_curr:
            g_mu_z_k_, g_var_z_k_, g_z_ = self.dist_gaussian(g_c_merge, 256, 'g_gaussian_k'+str(i)+'_task_'+str(task_idx))
            g_z_mu_k[task_idx] = g_mu_z_k_
            g_z_var_k[task_idx] = g_var_z_k_
            g_z_k[task_idx] = g_z_

        # --------------------------  Then Let's start Decoder Process --------------------------#

        ## dec_block4
        depth = [self.num_classes]*3
        merge4 = tf.concat(values=[conv4_clf, conv_comb], axis = -1)
        #merge4 = tf.add(conv4_clf, conv_comb)
        d_m_b4_out = self.residual_block(merge4, depth, 1, True, 4, 1, 4, name='dec_block')
        for unit_index in range(1, self.residual_units[3]):
          d_m_b4_out = self.residual_block(d_m_b4_out, depth, 1, False, 4, unit_index+1, 4, name='dec_block')
        with tf.variable_scope('dec_block4/unit_%d/bottleneck_v2/conv3'%(self.residual_units[3]+1)):
          d_m_b4_out = tf.nn.relu(self.batch_norm(d_m_b4_out))
        d_m_b4_out_up = tf.image.resize_nearest_neighbor(d_m_b4_out, (shape_b3[1], shape_b3[2]))

        ## dec_block3
        depth = [self.num_classes]*3
        merge3 = tf.add(conv3_clf, d_m_b4_out_up)
        d_m_b3_out = self.residual_block(merge3, depth, 1, True, 3, 1, 2, name='dec_block')
        for unit_index in range(1, self.residual_units[2]):
          d_m_b3_out = self.residual_block(d_m_b3_out, depth, 1, False, 3, unit_index+1, 2, name='dec_block')
        with tf.variable_scope('dec_block3/unit_%d/bottleneck_v2/conv3'%(self.residual_units[2]+1)):
          d_m_b3_out = tf.nn.relu(self.batch_norm(d_m_b3_out))
        d_m_b3_out_up = tf.image.resize_nearest_neighbor(d_m_b3_out, (shape_b2[1], shape_b2[2]))

         ## dec_block2
        depth = [self.num_classes]*3
        merge2 = tf.add(conv2_clf, d_m_b3_out_up)
        d_m_b2_out = self.residual_block(merge2, depth, 1, True, 2, 1, name='dec_block')
        for unit_index in range(1, self.residual_units[1]):
          d_m_b2_out = self.residual_block(d_m_b2_out, depth, 1, False, 2, unit_index+1, name='dec_block')
        with tf.variable_scope('dec_block2/unit_%d/bottleneck_v2/conv3'%(self.residual_units[1]+1)):
          d_m_b2_out = tf.nn.relu(self.batch_norm(d_m_b2_out))
        d_m_b2_out_up = tf.image.resize_nearest_neighbor(d_m_b2_out, (shape_b1[1], shape_b1[2]))

        ## dec_block1
        depth = [self.num_classes]*3
        merge1 = tf.add(conv1_clf, d_m_b2_out_up)
        d_m_b1_out = self.residual_block(merge1, depth, 1, True, 1, 1, name='dec_block')
        for unit_index in range(1, self.residual_units[0]):
          d_m_b1_out = self.residual_block(d_m_b1_out, depth, 1, False, 1, unit_index+1, name='dec_block')
        with tf.variable_scope('dec_block1/unit_%d/bottleneck_v2/conv3'%(self.residual_units[0]+1)):
          d_m_b1_out = tf.nn.relu(self.batch_norm(d_m_b1_out))
        #d_m_b1_out = tf.image.resize_nearest_neighbor(d_m_b1_out, (input_shape[1], input_shape[2]))
        deconv1_2 = d_m_b1_out

        #assert(False), [v.get_shape().as_list() for v in [deconv1_2]]
        # assert(False), deconv1_2.get_shape().as_list()

        softmax_ss , aux1_ss, aux2_ss, softmax_bd, softmax_cc, aux1_cc, aux2_cc, \
          aux1_el, aux2_el, softmax_el = [None for i in range(10)]

        # Semantic Segmentation
        aux1_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(d_m_b4_out, 1, 1, self.num_classes, name='conv_aux1_ss', relu=False), \
                                  [input_shape[1], input_shape[2]]))
        aux2_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(d_m_b2_out, 1, 1, self.num_classes, name='conv_aux2_ss', relu=False), \
                                  [input_shape[1], input_shape[2]]))
        aux3_ss = tf.nn.softmax(tf.image.resize_images(self.conv_batchN_relu(d_m_b3_out, 1, 1, self.num_classes, name='conv_aux3_ss', relu=False), \
                                  [input_shape[1], input_shape[2]]))

        g_tile_conv_ss = self.convert_fc_conv(z=[g_z_mu_k[0]], k=1, h=input_shape[1], w=input_shape[2], channels=32, name='g_tile_conv_ss')
        merge_ss = tf.concat([g_tile_conv_ss, deconv1_2, aux1_ss, aux2_ss, aux3_ss], axis = -1)
        ss_specific = self.aconv_batchN_relu(merge_ss, 5, 2, self.num_classes, name='specific_ss')
        pre_ss = self.conv_batchN_relu(ss_specific, 5, 1, self.num_classes, name='pre_ss')
        with tf.variable_scope('sem_segmentation'):
          conv_seg = self.conv2d(pre_ss, 8, 1, self.num_classes)
          #conv_seg = self.batch_norm(conv_seg) 
        #with tf.variable_scope('sem_segmentation'):
        #  conv_seg = self.conv2d(deconv1_2, 1, 1, self.num_classes)
        #  conv_seg = self.batch_norm(conv_seg) 

        softmax_ss = tf.nn.softmax(conv_seg) #conv_seg
        
        return prob_gmm, z_mu_k, z_var_k, g_z_mu_k, g_z_var_k, softmax_ss, aux1_ss, aux2_ss
        
    # pos_multilb=None
    def loss(self, prob_gmm_pred, prob_gmm_label, prior_z_mu, prior_z_var,logits_ss, label_ss, batch_sz,\
                    pos_z_mu=None, pos_z_var=None, g_mu=None, g_var=None,\
                    aux1_ss=None, aux2_ss=None):
        #self.loss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label*tf.log(self.softmax+1e-10), self.weights), axis=[3]))
        
        loss = tf.Variable(0.0, trainable=False, name='loss')
        loss_ss = tf.Variable(0.0, trainable=False, name='loss_ss')
        loss_ss_iou = tf.Variable(0.0, trainable=False, name='loss_ss_iou')
        loss_z = tf.Variable(0.0, trainable=False, name='loss_z')
        loss_prob_gmm = tf.Variable(0.0, trainable=False, name='loss_prob_gmm')
        loss_g = tf.Variable(0.0, trainable=False, name='loss_g')

        loss_ss = self.loss_func.loss_cross(softmax=logits_ss, label=label_ss, weights=self.weights)
        loss_ss_iou = self.loss_func.IoU_loss_multilabel(logits=logits_ss, labels=label_ss, \
                        num_classes=self.num_classes)
        loss += 1.1*(loss_ss + loss_ss_iou)/2.0
        #if self.has_aux_loss:
        aux_loss1_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux1_ss+1e-10), self.weights), axis=[3]))
        aux_loss2_ss = tf.reduce_mean(-tf.reduce_sum(tf.multiply(label_ss*tf.log(aux2_ss+1e-10), self.weights), axis=[3]))
        loss = loss + (0.6*aux_loss1_ss + 0.5*aux_loss2_ss)

        for i in range(self.K):
          if pos_z_mu == None:
            loss_z += self.loss_func.kl_gaussian(mean=prior_z_mu[i], var=prior_z_var[i], average=False)
          else:
            loss_z += self.loss_func._kl_divergence_gaussians(mean_pri=prior_z_mu[i], log_var_pri=prior_z_var[i], \
                                          mean_pos=pos_z_mu[i], log_var_pos=pos_z_var[i])
        loss_z /= self.K
        loss += loss_z

        #if g_mu is not None and g_var is not None:
        for task_idx in self.list_index_task_curr:
          loss_g += self.loss_func.kl_gaussian(mean=g_mu[task_idx], var=g_var[task_idx], average=False)
        loss_g /= len(self.list_index_task_curr)
        loss += 0.5*loss_g

        #https://www.jessicayung.com/mse-as-maximum-likelihood/
        #https://www.quora.com/Whats-the-relationship-between-mean-squared-error-and-likelihood
        #loss_prob_gmm = tf.losses.mean_squared_error(prob_gmm_label, prob_gmm_pred)
        loss_prob_gmm = tf.losses.sigmoid_cross_entropy(multi_class_labels=prob_gmm_label, logits=prob_gmm_pred, scope="sigmoid_cross_entropy")
        loss += 0.4*loss_prob_gmm

        tf.add_to_collection('losses', loss)
        
        return loss, loss_g, loss_prob_gmm, loss_z, loss_ss, loss_ss_iou


    #def create_optimizer(self):
    #    self.lr = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.decay_steps, power=self.power)
    #    self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.global_step)
    #    #self.train_op = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.histogram("histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()

    def build_graph(self, data, batch_size, label_ss=None, label_bd=None, label_cc=None, label_el=None):
        self.forward(data, batch_size)
        #tf.get_variable_scope().reuse_variables()
        #if self.training:
        #    self._create_loss(label_ss, label_bd, label_cc, label_el)

def main():
    print('Do Nothing')
   
if __name__ == '__main__':
    main()

