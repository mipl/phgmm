import numpy as np
import tensorflow as tf
#from tensorflow.python.training import moving_averages

class Network(object):
    def __init__(self):
        print('Network_Construction')

    def _setup(self, data):
        raise NotImplementedError("Implement this method.")

    def _create_loss(self, label):
        raise NotImplementedError("Implement this method.")

    def _create_optimizer(self):
        raise NotImplementedError("Implement this method.")

    def _create_summaries(self):
        raise NotImplementedError("Implement this method.")

    def build_graph(self, data, label=None):
        raise NotImplementedError("Implement this method.")

    def conv2d(self, inputs, kernel_size, stride, out_channels, name=None, padding='SAME'):
        in_channels = inputs.get_shape().as_list()[-1]
        weight_shape = [kernel_size, kernel_size, in_channels, out_channels]
        initializer = tf.contrib.layers.xavier_initializer()

        if self.initializer is 'he':
            n = kernel_size * kernel_size * in_channels
            std = np.sqrt(2.0 / n)
            initializer = tf.truncated_normal_initializer(stddev=std)

        if name is None:
            name = 'weights'

        strides = [1, stride, stride, 1]
        with tf.variable_scope(name):
            kernel = tf.get_variable('', weight_shape,
                                     initializer=initializer,
                                     trainable=self.training,
                                     dtype=self.float_type,
                                     regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay)
                                    )
        return tf.nn.conv2d(inputs, kernel, strides=strides, padding=padding)

    def split_conv2d(self, inputs, kernel_size, rate, out_channels, name=None, padding='SAME', both_atrous=False):
        in_channels = inputs.get_shape().as_list()[-1]
        weight_shape = [kernel_size, kernel_size, in_channels, out_channels]
        initializer = tf.contrib.layers.xavier_initializer()
        if self.initializer is 'he':
            n = kernel_size * kernel_size * in_channels
            std = np.sqrt(2.0 / n)
            initializer = tf.truncated_normal_initializer(stddev=std)
        if name is None:
            name = 'weights'

        strides = [1, 1, 1, 1]
        with tf.variable_scope(name):
            kernel = tf.get_variable('', weight_shape,
                                     initializer=initializer,
                                     trainable=self.training,
                                     dtype=self.float_type,
                                     regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay)
                                    )
        kernelA, kernelB = tf.split(kernel, 2, 3)
        if both_atrous:
            outA = tf.nn.atrous_conv2d(inputs, kernelA, rate, padding=padding)
            outB = tf.nn.atrous_conv2d(inputs, kernelB, rate, padding=padding)
        else:
            outA = tf.nn.conv2d(inputs, kernelA, strides=strides, padding=padding)
            outB = tf.nn.atrous_conv2d(inputs, kernelB, rate, padding=padding)
        return tf.concat((outA, outB), 3)

    def batch_norm(self, inputs):
        in_channels = inputs.get_shape().as_list()[-1]

        with tf.variable_scope('BatchNorm'):
            gamma = tf.get_variable('gamma', (in_channels,), initializer=tf.constant_initializer(1.0),
                                    trainable=self.training, dtype=self.float_type,
                                    regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay))
            beta = tf.get_variable('beta', (in_channels,), initializer=tf.constant_initializer(0),
                                   trainable=self.training, dtype=self.float_type,
                                   regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay))
            moving_mean = tf.get_variable('moving_mean', (in_channels,),
                                          initializer=tf.constant_initializer(0), trainable=False, dtype=self.float_type)
            moving_var = tf.get_variable('moving_variance', (in_channels,),
                                         initializer=tf.constant_initializer(1), trainable=False, dtype=self.float_type)

        if self.training:
            batch_mean, batch_var = tf.nn.moments(inputs, [0, 1, 2])
            #batch_norm, https://github.com/tensorflow/models/blob/master/research/inception/inception/slim/ops.py
            train_mean = tf.assign(moving_mean, moving_mean* self.bn_decay_ + batch_mean * (1 - self.bn_decay_))
            train_var = tf.assign(moving_var, moving_var * self.bn_decay_ + batch_var * (1 - self.bn_decay_))
            tf.add_to_collection('_update_ops_', train_mean)
            tf.add_to_collection('_update_ops_', train_var)
            with tf.control_dependencies([train_mean, train_var]):
                return  tf.nn.batch_normalization(inputs, batch_mean, batch_var, scale=gamma, offset=beta, variance_epsilon=0.00001)
        else:
            return  tf.nn.batch_normalization(inputs, moving_mean, moving_var, scale=gamma, offset=beta, variance_epsilon=0.00001)

    def pool(self, x, k_size, strides, name=None, padding='SAME'):
        return tf.nn.max_pool(x, [1, k_size, k_size, 1], strides=[1, strides, strides, 1], padding=padding)

    def ave_pool(self, x, k_size, strides, name=None, padding='SAME'):
        return tf.nn.avg_pool(x, [1, k_size, k_size, 1], strides=[1, strides, strides, 1], padding=padding)

    def upsample(self, inputs, h, w, name=None):
        #H, W, _ = inputs.get_shape().as_list()[1:]
        #tf.image.resize_bilinear
        #tf.image.resize_bicubic
        #tf.image.resize_area
        return tf.image.resize_nearest_neighbor(inputs, (h, w), name="upsample_{}".format(name))
    
    def resize_tensor(self, inputs, h, w, name=None):
        #H, W, _ = inputs.get_shape().as_list()[1:]
        #tf.image.resize_bilinear
        #tf.image.resize_bicubic
        #tf.image.resize_area
        return tf.image.resize_nearest_neighbor(inputs, (h, w), name="resize_{}".format(name))

    def pool_index(self, x, k_size, strides, name=None,  padding='SAME'):
        value, index = tf.nn.max_pool_with_argmax(tf.to_double(x), \
        ksize=[1, k_size, k_size, 1], strides=[1, strides, strides, 1], padding=padding)
        return tf.to_float(value), index, x.get_shape().as_list()

    def up_sampling(self, pool, ind, output_shape, batch_size, name=None):
       #with tf.variable_scope(name):
        pool_ = tf.reshape(pool, [-1])
        batch_range = tf.reshape(tf.range(batch_size, dtype=ind.dtype), [tf.shape(pool)[0], 1, 1, 1])
        b = tf.ones_like(ind) * batch_range
        b = tf.reshape(b, [-1, 1])
        ind_ = tf.reshape(ind, [-1, 1])
        ind_ = tf.concat([b, ind_], 1)
        ret = tf.scatter_nd(ind_, pool_, shape=[batch_size, output_shape[1] * output_shape[2] * output_shape[3]])
        ret = tf.reshape(ret, [tf.shape(pool)[0], output_shape[1], output_shape[2], output_shape[3]])
        return ret

    def atrous(self, inputs, kernel_size, rate, out_channels, name=None, padding='SAME'):
        in_channels = inputs.get_shape().as_list()[-1]
        weight_shape = [kernel_size, kernel_size, in_channels, out_channels]
        initializer = tf.contrib.layers.xavier_initializer()

        if self.initializer is 'he':
            n = kernel_size * kernel_size * in_channels
            std = np.sqrt(2.0 / n)
            initializer = tf.truncated_normal_initializer(stddev=std)

        if name is None:
            name = 'weights'

        with tf.variable_scope(name):
            kernel = tf.get_variable('', weight_shape,
                                     initializer=initializer,
                                     trainable=self.training,
                                     dtype=self.float_type,
                                     regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay)
                                    )
        return tf.nn.atrous_conv2d(inputs, kernel, rate, padding=padding)

    def tconv2d(self, inputs, kernel_size, out_channels, stride, h, w, name=None, padding='SAME'):
        in_channels = inputs.get_shape().as_list()[-1]
        weight_shape = [kernel_size, kernel_size, out_channels, in_channels]
        n = kernel_size * kernel_size * in_channels
        std = np.sqrt(2.0 / n)

        if name is None:
            name = 'weights'

        initializer = tf.truncated_normal_initializer(stddev=std)
        with tf.variable_scope(name):
            kernel = tf.get_variable('', weight_shape,
                                     initializer=initializer,
                                     trainable=self.training,
                                     dtype=self.float_type,
                                     regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay)
                                    )

        in_shape = tf.shape(inputs)
        #h = in_shape[1]*stride
        #w = in_shape[2]*stride 
        new_shape = [in_shape[0], h, w, out_channels]
        return tf.nn.conv2d_transpose(inputs, kernel, new_shape, strides=[1, stride, stride, 1], padding=padding)

    def conv_batchN_relu(self, x, kernel_size, stride, out_channels, name, relu=True, dropout=False):
        with tf.variable_scope(name):
            conv_out = self.conv2d(x, kernel_size, stride, out_channels)
            if dropout:
                conv_out = tf.nn.dropout(conv_out, self.keep_prob)
            conv_batch_norm_out = self.batch_norm(conv_out)
            if relu:
                conv_activation = tf.nn.relu(conv_batch_norm_out)
                return conv_activation
            else:
                return conv_batch_norm_out
    
    def conv_batchN_act(self, x, kernel_size, stride, out_channels, name, act=None, dropout=False):
        with tf.variable_scope(name):
            conv_out = self.conv2d(x, kernel_size, stride, out_channels)
            if dropout:
                conv_out = tf.nn.dropout(conv_out, self.keep_prob)
            conv_batch_norm_out = self.batch_norm(conv_out)
            if act is not None:
                conv_activation = act(conv_batch_norm_out)
                return conv_activation
            else:
                return conv_batch_norm_out
    
    def conv_batchN_relu_res(self, x, kernel_size, stride, out_channels, name, relu=True, dropout=False):
        '''conv_batchN_relu residual block'''
        with tf.variable_scope(name):
            conv_out = self.conv2d(x, kernel_size, stride, out_channels)
            if dropout:
                conv_out = tf.nn.dropout(conv_out, self.keep_prob)
            conv_batch_norm_out = self.batch_norm(conv_out) + x
            if relu:
                conv_activation = tf.nn.relu(conv_batch_norm_out)
                return conv_activation
            else:
                return conv_batch_norm_out

    def tconv_batchN_relu(self, x, kernel_size, stride, out_channels, h, w, name, relu=True, dropout=False):
        with tf.variable_scope(name):
            tconv_out = self.tconv2d(x, kernel_size, out_channels, stride, h, w)
            if dropout:
                tconv_out = tf.nn.dropout(tconv_out, self.keep_prob)
            tconv_batch_norm_out = self.batch_norm(tconv_out)
            if relu:
               tconv_activation = tf.nn.relu(tconv_batch_norm_out)
               return tconv_activation
            else:
                return tconv_batch_norm_out

    def aconv_batchN_relu(self, x, kernel_size, rate, out_channels, name, relu=True):
        with tf.variable_scope(name):
            conv_out = self.atrous(x, kernel_size, rate, out_channels)
            conv_batch_norm_out = self.batch_norm(conv_out)
            if relu:
                conv_activation = tf.nn.relu(conv_batch_norm_out)
                return conv_activation
            else:
                return conv_batch_norm_out

    def unit_0(self, x, out_channels, block, unit):
        with tf.variable_scope('block%d/unit_%d/bottleneck_v1'%(block, unit)):
            b_u_1r_cout = self.conv_batchN_relu(x, 1, 1, out_channels/4, name='conv1')
            b_u_3_cout = self.conv_batchN_relu(b_u_1r_cout, 3, 1, out_channels/4, name='conv2')
            with tf.variable_scope('conv3'):
                b_u_1e_cout=self.conv2d(b_u_3_cout, 1, 1, out_channels)

            with tf.variable_scope('shortcut'):
                b_u_s_cout = self.conv2d(b_u_1r_cout, 1, 1, out_channels)

            b_u_out = tf.add(b_u_1e_cout, b_u_s_cout)
        return b_u_out    
    
    def unit_1(self, x, out_channels, stride, block, unit, shortcut=False):
        if shortcut == False:
            with tf.variable_scope('block%d/unit_%d/bottleneck_v1/conv3'%(block, unit-1)):
                x_bn = tf.nn.relu(self.batch_norm(x))
        else:
                x_bn = x
        with tf.variable_scope('block%d/unit_%d/bottleneck_v1'%(block, unit)):
            b_u_1r_cout = self.conv_batchN_relu(x_bn, 1, 1, out_channels/4, name='conv1')
            b_u_3_cout = self.conv_batchN_relu(b_u_1r_cout, 3, stride, out_channels/4, name='conv2')
            with tf.variable_scope('conv3'):
                b_u_1e_cout = self.conv2d(b_u_3_cout, 1, 1, out_channels)

            if shortcut:
                with tf.variable_scope('shortcut'):
                    b_u_s_cout = self.conv2d(x_bn, 1, stride, out_channels)
                    b_u_out = tf.add(b_u_1e_cout, b_u_s_cout)
            else:
                b_u_out = tf.add(b_u_1e_cout, x)
             
            return b_u_out

    def unit_3(self, x, out_channels, block, unit):
        with tf.variable_scope('block%d/unit_%d/bottleneck_v1/conv3'%(block, unit-1)):
            x_bn = tf.nn.relu(self.batch_norm(x))
        with tf.variable_scope('block%d/unit_%d/bottleneck_v1'%(block, unit)):
            b_u_1r_cout = self.conv_batchN_relu(x_bn, 1, 1, out_channels/4, name='conv1')
            with tf.variable_scope('conv2'):
                b3_u3_3_cout = self.split_conv2d(b_u_1r_cout, 3, 2, out_channels/4)
                b3_u3_3_bnout = self.batch_norm(b3_u3_3_cout)
                b3_u3_3_ract = tf.nn.relu(b3_u3_3_bnout)

            with tf.variable_scope('conv3'):
                b3_u3_1e_cout = self.conv2d(b3_u3_3_ract, 1, 1, out_channels)
            b3_u3_out = tf.add(x, b3_u3_1e_cout)

        return b3_u3_out

    def unit_4(self, x, out_channels, block, unit, shortcut=False, dropout=False):
        if shortcut:
            with tf.variable_scope('block%d/unit_%d/bottleneck_v1/conv3'%(block-1, 6)):
                x_bn = tf.nn.relu(self.batch_norm(x))
        else:
            with tf.variable_scope('block%d/unit_%d/bottleneck_v1/conv3'%(block, unit-1)):
                x_bn = tf.nn.relu(self.batch_norm(x))

        with tf.variable_scope('block%d/unit_%d/bottleneck_v1'%(block, unit)):
            b_u_1r_cout = self.conv_batchN_relu(x_bn, 1, 1, out_channels/4, name='conv1')
            with tf.variable_scope('conv2'):
                b3_u3_3_cout = self.split_conv2d(b_u_1r_cout, 3, 2, out_channels/4, both_atrous=True)
                b3_u3_3_bnout = self.batch_norm(b3_u3_3_cout)
                b3_u3_3_ract = tf.nn.relu(b3_u3_3_bnout)

            with tf.variable_scope('conv3'):
                b3_u3_1e_cout=self.conv2d(b3_u3_3_ract, 1, 1, out_channels)

            if dropout:
        	      b3_u3_1e_cout=tf.nn.dropout(b3_u3_1e_cout, self.keep_prob)

            if shortcut:
                with tf.variable_scope('shortcut'):
                    b_u_s_cout = self.conv2d(x_bn, 1, 1, out_channels)
                    b3_u3_out = tf.add(b_u_s_cout, b3_u3_1e_cout)
            else:
                b3_u3_out = tf.add(x, b3_u3_1e_cout)

        return b3_u3_out

    def fc(self, inputs, out_channels, name):
        ''' inputs: [None, H x W x C] '''
        in_channels = inputs.get_shape().as_list()[-1]
        weight_shape = [in_channels, out_channels]
        initializer = tf.contrib.layers.xavier_initializer()
        if self.initializer is 'he':
            n = in_channels
            std = np.sqrt(2.0 / n)
            initializer = tf.truncated_normal_initializer(stddev=std)
        name_W = name+'/weights'
        
        with tf.variable_scope(name_W):
            kernel = tf.get_variable('', weight_shape,
                                     initializer=initializer,
                                     trainable=self.training,
                                     dtype=self.float_type, 
                                     regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay))
            x = tf.matmul(inputs, kernel)
        with tf.variable_scope(name): 
            b = tf.get_variable('biases', [out_channels], trainable=self.training,
                                initializer=tf.constant_initializer(0.02), dtype=self.float_type)
        return tf.nn.bias_add(x, b, data_format='NHWC')

    def conv_bias(self, inputs, kernel_size, stride, out_channels, name):
        with tf.variable_scope(name):
            x = self.conv2d(inputs, kernel_size, stride, out_channels)
            b = tf.get_variable('biases', [out_channels], trainable=self.training,
                                initializer=tf.constant_initializer(0.01), dtype=self.float_type)
        return tf.nn.bias_add(x, b, data_format='NHWC')

    def tconv_bias(self, inputs, kernel_size, stride, out_channels, h, w, name):
        with tf.variable_scope(name):
            x = self.tconv2d(inputs, kernel_size, stride, out_channels, h, w)
            b = tf.get_variable('biases', [out_channels], trainable=self.training,
                                initializer=tf.constant_initializer(0.01), dtype=self.float_type)
        return tf.nn.bias_add(x, b, data_format='NHWC')

    #def residual_block_v2(self, inputs, depth, depth_bottleneck, stride, rate, block, unit):
    def residual_block(self, x, list_channels, stride, change_dimension, block, unit, rate=1, name='enc_block'):
        #https://github.com/sthalles/deeplab_v3/blob/master/resnet/resnet_v2.py
        with tf.variable_scope(name+'%d/unit_%d/bottleneck_v2'%(block, unit)):
            if change_dimension:
                with tf.variable_scope('shortcut'):
                    short_cut_conv = self.conv2d(x, 1, stride, list_channels[2])#, name="shortcut")
                    block_conv_input  = self.batch_norm(short_cut_conv)
            else:
                block_conv_input = x

            b_u_1_cout = self.conv_batchN_relu(x, 1, stride, list_channels[0], name='conv1')
            if rate == 1:
                b_u_3_cout = self.conv_batchN_relu(b_u_1_cout, 3, 1, list_channels[1], name='conv2')
            else:
                b_u_3_cout = self.aconv_batchN_relu(b_u_1_cout, 3, rate, list_channels[1], name='aconv2')
            with tf.variable_scope('conv3'):
                b_u_1e_cout = self.conv2d(b_u_3_cout, 1, 1, list_channels[2])
                #self.batch_norm(b_u_1e_cout)
            b_u_out = tf.add(block_conv_input, b_u_1e_cout)
        return b_u_out

    def ASPP(self, inputs, filters=256, name=None):
        '''
        Atrous Spatial Pyramid Pooling (ASPP) Block
        '''
        with tf.variable_scope(name):
            pool_height = tf.shape(inputs)[1]
            pool_width = tf.shape(inputs)[2]

            resize_height = pool_height
            resize_width = pool_width

            # Atrous Spatial Pyramid Pooling
            # Atrous 1x1
            aspp1x1 = self.conv2d(inputs, 1, 1, filters, name='aspp1x1')
            # Atrous 3x3, rate = 6
            aspp3x3_1 = self.atrous(inputs, 3, 6, filters, name='aspp3x3_1')
            # Atrous 3x3, rate = 12
            aspp3x3_2 = self.atrous(inputs, 3, 12, filters, name='aspp3x3_2')
            # Atrous 3x3, rate = 18
            aspp3x3_3 = self.atrous(inputs, 3, 18, filters, name='aspp3x3_3')

            # Image Level Pooling,  global average pooling
            image_feature = tf.reduce_mean(inputs, [1, 2], keepdims=True)
            image_feature = self.conv2d(image_feature, 3, 1, filters)
            image_feature = tf.image.resize_bilinear(images=image_feature, size=[resize_height, resize_width], align_corners=True, name='image_pool_feature')

            # Merge Poolings
            outputs = tf.concat(values=[aspp1x1, aspp3x3_1, aspp3x3_2, aspp3x3_3, image_feature], axis=3, name='aspp_pools')
            outputs = self.conv2d(outputs, 1, 1, filters, name='aspp_outputs')

        return outputs
    
    def compress_feature(self, x):
        """compress from [batch,h,w,c] to [batch,c]"""
        x = tf.reduce_mean(x, axis=1)
        x = tf.reduce_mean(x, axis=1)
        return x

    def dist_gaussian(self, hidden, gaussian_size, name):
        """Sample from the Gaussian distribution
        
        Args:
          hidden: (array) [batch_size, n_features] features obtained by the encoder
          gaussian_size: (int) size of the gaussian sample vector
          
        Returns:
          (dict) contains the nodes of the mean, log of variance and gaussian
        """
        eps = 1e-6
        with tf.variable_scope(name): #, reuse=tf.AUTO_REUSE):
          out = hidden
          #mean = tf.layers.dense(out, units=gaussian_size, name='mean')
          #var = tf.layers.dense(out, units=gaussian_size, activation=tf.nn.softplus, name='var')
          mean = self.fc(inputs=out, out_channels=gaussian_size, name=name+'_mean')
          #var = self.fc(inputs=out, out_channels=gaussian_size, name=name+'_var')
          var = tf.nn.softplus(self.fc(inputs=out, out_channels=gaussian_size, name=name+'_var'))
          noise = tf.random_normal(tf.shape(mean), mean = 0, stddev = 0.02, dtype= tf.float32)
          z = mean + tf.sqrt(var + eps) * noise
          #return {'mean': mean, 'var': var, 'gaussian': z}
          return mean, var, z
    
    def dist_gaussian_conv(self, hidden, gaussian_size, name):
        """Sample from the Gaussian distribution
        
        Args:
          hidden: (array) [batch_size, h, w] features obtained by the encoder
          gaussian_size: (int) size of the gaussian sample vector
          
        Returns:
          (dict) contains the nodes of the mean, log of variance and gaussian
        """
        hidden = tf.expand_dims(hidden, axis=-1)
        sz = hidden.get_shape().as_list()
        bt_sz = tf.shape(hidden)[0]
        assert(gaussian_size == sz[1]*sz[2]) #GMMBlock
        eps = 1e-6
        with tf.variable_scope(name): #, reuse=tf.AUTO_REUSE):
          out = hidden
          #mean = tf.layers.dense(out, units=gaussian_size, name='mean')
          #var = tf.layers.dense(out, units=gaussian_size, activation=tf.nn.softplus, name='var')
          #mean = self.fc(inputs=out, out_channels=gaussian_size, name=name+'_mean')
          mean = self.conv2d(out, 5, 1, 1, name=name+'_mean')
          mean = tf.reshape(mean, [bt_sz, sz[1]*sz[2]])
          var = self.conv2d(out, 3, 1, 1, name=name+'_var')
          var = tf.nn.softplus(tf.reshape(var, [bt_sz, sz[1]*sz[2]]))
          noise = tf.random_normal(tf.shape(mean), mean = 0, stddev = 0.02, dtype= tf.float32)
          z = mean + tf.sqrt(var + eps) * noise
          #return {'mean': mean, 'var': var, 'gaussian': z}
          return mean, var, z
    
    def dist_gaussian_conv_up(self, hidden, h, w, name):
        """Sample from the Gaussian distribution
        
        Args:
          hidden: (array) [batch_size, h, w] features obtained by the encoder
          gaussian_size: (int) size of the gaussian sample vector, h*w
          
        Returns:
          (dict) contains the nodes of the mean, log of variance and gaussian
        """
        hidden = tf.expand_dims(hidden, axis=-1)
        #sz = hidden.get_shape().as_list()
        bt_sz = tf.shape(hidden)[0]
        #assert(gaussian_size == sz[1]*2*sz[2]*2) #GMMUpScale
        eps = 1e-6
        with tf.variable_scope(name): #, reuse=tf.AUTO_REUSE):
          out = hidden
          #mean = tf.layers.dense(out, units=gaussian_size, name='mean')
          #var = tf.layers.dense(out, units=gaussian_size, activation=tf.nn.softplus, name='var')
          #mean = self.fc(inputs=out, out_channels=gaussian_size, name=name+'_mean')
          mean = self.tconv2d(out, 5, 1, 2, h, w, name=name+'_mean')
          #assert(False), [v.get_shape().as_list() for v in [mean, out]]
          mean = tf.reshape(mean, [bt_sz, h*w])
          var = self.tconv2d(out, 4, 1, 2, h, w, name=name+'_var')
          var = tf.nn.softplus(tf.reshape(var, [bt_sz, h*w]))
          noise = tf.random_normal(tf.shape(mean), mean = 0, stddev = 0.02, dtype= tf.float32)
          z = mean + tf.sqrt(var + eps) * noise
          #return {'mean': mean, 'var': var, 'gaussian': z}
          return mean, var, z

    def uncompress_feature(self, z, h, w):
        """uncompress from [batch,c] to [batch,h,w,c]"""
        _spatial_axes = [1,2]
        #shp = features.get_shape()
        #spatial_shape = [shp[axis] for axis in _spatial_axes]
        #multiples = [1] + spatial_shape + [1]
        multiples = [1, h, w, 1]
        z = tf.expand_dims(z, axis=1)
        z = tf.expand_dims(z, axis=1)
        # broadcast latent vector to spatial dimensions of the image[h,w] tensor
        broadcast_z = tf.tile(z, multiples)
        return broadcast_z

    def convert_fc_conv(self, z, k, h, w, channels, name):
        """convert fully connected to convolutional layer
        z is a list of k clusters """
        with tf.variable_scope(name): #, reuse=tf.AUTO_REUSE):
          xk_list = [None] * k
          for i in range(k):
            z_k = self.fc(inputs=z[i], out_channels=channels, name=name+'_fc2conv'+str(i))
            z_k = tf.nn.relu(z_k)
            x_k = self.uncompress_feature(z_k, h, w)
            xk_list[i] = x_k
          x = tf.concat(xk_list, -1)
          return x

    # ------------------ Graph Neural Network -----------------

    def batch_matmul(self, x, w):
        """matrix multiply of [?,x,y] * [y,z] = [?,x,z]"""
        sz_x  = x.get_shape().as_list()
        sz_w  = w.get_shape().as_list()
        x_ = tf.reshape(x, [-1, sz_x[2]])
        y_ = tf.matmul(x_, w)
        y = tf.reshape(y_, [-1, sz_x[1], sz_w[1]] )
        return y

    def batch_matmul2(self, x, y, bt_sz):
        """matrix multiply of [b,x,y] * [b,y,z] = [b,x,z]"""
        res = list()
        for i in range(bt_sz):
          res.append( tf.matmul(x[i], y[i]) )
        res = tf.stack( res )
        return res
      
    def batch_spasematmul(self, x, y, bt_sz):
        """matrix multiply of sparse[n,n] * [?,n,z] = [?,n,z]"""
        res = list()
        for i in range(bt_sz):
          res.append( tf.sparse_tensor_dense_matmul(x, y[i]) )
        res = tf.stack( res )
        return res

    def dot(self, x, y, bt_sz, sparse=False):
        """Wrapper for tf.matmul (sparse vs dense)."""
        if sparse:        
            #res = tf.sparse_tensor_dense_matmul(x, y[0])
            res = self.batch_spasematmul(x, y, bt_sz)
            #list_t = [x, y, res]
            #assert(False), [t.get_shape() for t in list_t]
        else:
            #res = tf.matmul(x, y)
            res = self.batch_matmul(x, y)
        return res

    def GraphConvolution(self, inputs, input_dim, output_dim, bt_sz, support, dropout_value, dropout=0., \
                 sparse_inputs=False, act=tf.nn.relu, bias=False, name=None):
        with tf.variable_scope(name + '_vars'):
            sz_in = inputs.get_shape().as_list()

            if not dropout:
                dropout_value = 0.

            list_weights = [None] * len(support)

            for i in range(len(support)):
                #list_weights[i] = glorot([input_dim, output_dim], name='weights_' + str(i)) #(1433, 16)
                weight_shape = [input_dim, output_dim]
                initializer = tf.contrib.layers.xavier_initializer()

                list_weights[i] = tf.get_variable('weights', weight_shape, initializer=initializer,
                                         trainable=self.training,
                                         dtype=self.float_type,
                                         regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay)
                                        )
            if bias:
                bias_value = tf.get_variable('biases', [output_dim], trainable=self.training,
                                    initializer=tf.constant_initializer(0.01), dtype=self.float_type)
                #bias_value = zeros([output_dim], name='bias')

            x = inputs #(2708, 1433)
            x = tf.nn.dropout(x, 1-dropout_value)

            # convolve
            supports = list()
            for i in range(len(support)):
                HW = self.dot(x, list_weights[i], bt_sz, sparse=False)
                AHW = self.dot(support[i], HW, bt_sz, sparse=True)
                supports.append(AHW)
            output = tf.add_n(supports) #(?,16)

            # bias
            if bias:
                output += bias_value
            output = tf.reshape(output, [bt_sz, sz_in[1], output_dim])
            
            if act is not None:
                output = act(output)
            return output
        
    def Dense( self, inputs, input_dim, output_dim, bt_sz, dropout_value, dropout=0.,\
                 sparse_inputs=False, act=tf.nn.relu, bias=False, featureless=False, name=None):
        """Dense layer."""

        with tf.variable_scope(name + '_vars'):
            if not dropout:
              dropout_value = 0.

            #weights_value = glorot([input_dim, output_dim], name='weights')
            weight_shape = [input_dim, output_dim]
            initializer = tf.contrib.layers.xavier_initializer()
            weights_value = tf.get_variable('weights', weight_shape, initializer=initializer,
                                     trainable=self.training,
                                     dtype=self.float_type,
                                     regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay)
                                    )

            if bias:
                bias_value = tf.get_variable('biases', [output_dim], trainable=self.training,
                                    initializer=tf.constant_initializer(0.01), dtype=self.float_type)
                #bias_value = zeros([output_dim], name='bias')

            x = inputs

            x = tf.nn.dropout(x, 1.0-dropout_value)

            # transform
            output = self.dot(x, weights_value, bt_sz, sparse=sparse_inputs)

            # bias
            if bias:
                output += bias_value

            if act is not None:
                output = act(output)
            return output