import argparse
import datetime
import importlib
import os
import sys
import numpy as np
import tensorflow as tf
import yaml
from tqdm import tqdm
from PIL import Image
from dataset.helper import *
from models.PHGMM import PHGMM
from summary.summary import CTensorBoard

from dataset.Cityscape_read import CITYSCAPES
from dataset.Synthia_read import SYNTHIA

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-c', '--config', default='config/cityscapes_test.config')

def test_func(config):
    os.environ['CUDA_VISIBLE_DEVICES'] = config['gpu_id']
    name_model = config['model']

    # placeholders
    batch_size_pl = tf.placeholder(tf.int64, shape=[], name="batch_size")
    images_pl = tf.placeholder(tf.float32, [None, config['height'], config['width'], 3])
    labels_S_pl1 = tf.placeholder(tf.float32, [None, config['height'], config['width'], 1])
    labels_S_pl = tf.one_hot(tf.cast(tf.squeeze(labels_S_pl1,axis=-1),tf.int64), config['num_classes'])

    with tf.variable_scope(name_model):
        model = PHGMM(num_classes=config['num_classes'], \
                float_type=tf.float32, training=False, ignore_label=True, has_aux_loss=False)
        prob_gmm, z_prior_mu, z_prior_var, g_mu, g_var, logits_S, aux1_S, aux2_S \
          = model.forward(images_pl, config['batch_size'])

    config1 = tf.ConfigProto()
    config1.gpu_options.allow_growth = True
    sess = tf.Session(config=config1)
    sess.run(tf.global_variables_initializer())
    import_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    print('total_variables_loaded:', len(import_variables))
    from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
    saver = tf.train.Saver(import_variables)
    saver.restore(sess, config['checkpoint'])

    if config['dataset'] == 'cityscape_11':
      num_classes = config['num_classes']  # 11+1
      energy_level = 6 # 5+1 
      # /work/Cityscapes_aug3/train   17850 # 17850 - (17850 % config['batch_size']*len(devices))
      # /work/Cityscapes_aug3/val     500
      # /work/Cityscapes_aug3/test    1525
      dataset = CITYSCAPES(input_paste="/drw/Cityscapes_aug3", \
                    num_classes=num_classes, energy_level=energy_level-1, use_threads=12*4, num_data_load=\
                    #17850 - (17850 % (config['batch_size']*len(devices)) ) )
                    150)
      dataset.load_data(size=(384,768))
      #mean_channel = dataset.get_mean_channel()
    elif config['dataset'] == 'synthia':
      num_classes = config['num_classes']  # 11+1
      energy_level = 6 # 5+1
      #input_paste="/datasets/synthia/", 9400 (7k train) (2.4k test)
      #input_paste="/datasets/synthia/Rand/", 7000
      dataset = SYNTHIA(input_paste="/datasets/SYNTHIA/Rand", num_classes=num_classes, \
              energy_level=energy_level-1, use_threads=12*4, regression=False, num_data_load=\
              150 )
      dataset.load_data(size=(384,768)) 
    else:
      sys.exit('dataset undefined, [cityscape_11, synthia]')

    step = 0
    total_num = 0
    output_matrix_S = np.zeros([config['num_classes'], 3])
    
    print(config['model'] + " --- " + config['dataset'] + " --- " )

    bt_sz = config['batch_size']
    testing_set = None
    if config['dataset'] == 'synthia':
      testing_set = 'val'
    elif config['dataset'] == 'cityscape_11':
      testing_set = 'val'
    else:
      sys.exit('dataset undefined, [cityscape_11, synthia]')

    total_batch = int(dataset.get_size_dataset(testing_set)/bt_sz)
    pbar = tqdm(total=int(dataset.get_size_dataset(testing_set)))
    for i in range(total_batch):
      batch_img, batch_gt_S, batch_gt_E, batch_gt_C, batch_gt_D, \
          batch_nameimg = dataset.next_batch(state=testing_set, batch_size=bt_sz)
      if len(batch_img) == 0:
        continue

      pbar.update(batch_gt_S.shape[0])
      for j in range(len(batch_nameimg)):
        batch_nameimg[j] = batch_nameimg[j].replace('.jpg','.png')
      batch_gt_S_ = np.expand_dims(batch_gt_S, axis=-1)

      feed_dict = {images_pl:batch_img, labels_S_pl1:batch_gt_S_, batch_size_pl:bt_sz}
      fetches = [ logits_S ] 

      res = sess.run(fetches, feed_dict)
      prob_S = res[0]

      prediction_S = np.argmax(prob_S, 3)
      gt_S = batch_gt_S
      prediction_S[gt_S == config['unlabel']] = config['unlabel']
      output_matrix_S = compute_output_matrix(gt_S, prediction_S, output_matrix_S)

      # save image rgb, prediction and ground thuth
      '''
      for k in range(len(batch_img)):
        paste_out_pred = config['saved_results']+'/seg_pred/'+batch_nameimg[k]
        dataset.writeImage(prediction_S[k].astype(np.uint8), paste_out_pred) #save predicted segmentation

        paste_img = name_cam = config['saved_results']+'/img/'+batch_nameimg[k]
        bt_img = Image.fromarray(batch_img[k].astype(np.uint8))
        bt_img.save(paste_img)

        paste_gt = name_cam = config['saved_results']+'/'+tasks+'/seg_gt/'+batch_nameimg[k]
        dataset.writeImage(gt_S[k].astype(np.uint8), paste_gt)
      '''

      step += 1
    sess.close()
    pbar.close()
    print("------------------------------------------")
    print('SmIoU: ', compute_iou(output_matrix_S, config['unlabel'], True))

def main():
    args = PARSER.parse_args()
    if args.config:
        file_address = open(args.config)
        config = yaml.load(file_address)
    else:
        print('--config config_file_address missing')
    test_func(config)

if __name__ == '__main__':
    main()
