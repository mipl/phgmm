# Global and Local Features through Gaussian Mixture Models on Image Semantic Segmentation
Created by Darwin Saire and Adín Ramírez Rivera

### Model

<div align="center"><img src="img/model_PHGMM.png" alt="non-trivial image" width="85%" align="center"></div> <br>

Given an input image, we extract local, $`z`$, and global, $`g`$, features to improve the prediction of the semantic classes. We model the local features as a GMM that is fit to a predicted posterior from the data. For the global features, we use a single Gaussian to hold the holistic information of the image. The whole process is regularized by making the predictions for matching the ground truth, $`\ell_s`$, and making the local and global spaces similar to their respective priors, $`\ell_z`$ and $`\ell_g`$, respectively. We show that by endowing the latent spaces ($`z`$ and $`g`$) with clustering behavior and providing them with a structural, we improved the results of the SS task in the Cityscapes and Synthia datasets by enhancing the state-of-the-art without any refinement post-processing. You can see our paper [here](https://doi.org/10.1109/ACCESS.2022.3192605).

We provide the latent space of a structure through a mixture of Gaussians, producing it with clustering behavior.

| iteration ≈  $`10^2`$ | iteration ≈ $`10^3`$ | iteration ≈ $`10^4`$ | iteration ≈ $`10^5`$ |
| ----------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="img/citys_state0.png" width="100%" align="center"> | <img src="img/citys_state1.png" width="100%" align="center"> | <img src="img/citys_state2.png" width="100%" align="center"> | <img src="img/citys_state3.png" width="100%" align="center"> |

Following, our qualitative results.

<div align="center">
<table cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="img/img_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/gt_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/ocnet_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/dunet_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/hrnet_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/hrnet+ocr_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/phgmm_frankfurt_000000_014480_leftImg8bit.png" width="100%" align="center"></td>
  </tr>
  <tr>
    <td><img src="img/img_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/gt_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/ocnet_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/dunet_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/hrnet_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/hrnet+ocr_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
    <td><img src="img/phgmm_munster_000012_000019_leftImg8bit.png" width="100%" align="center"></td>
  </tr>
  <tr>
    <td><img src="img/img_0000099.png" width="100%" align="center"></td>
    <td><img src="img/gt_0000099.png" width="100%" align="center"></td>
    <td><img src="img/ocnet_0000099.png" width="100%" align="center"></td>
    <td><img src="img/dunet_0000099.png" width="100%" align="center"></td>
    <td><img src="img/hrnet_0000099.png" width="100%" align="center"></td>
    <td><img src="img/hrnet+ocr_0000099.png" width="100%" align="center"></td>
    <td><img src="img/phgmm_0000099.png" width="100%" align="center"></td>
  </tr>
  <tr>
    <td><img src="img/img_0000465.png" width="100%" align="center"></td>
    <td><img src="img/gt_0000465.png" width="100%" align="center"></td>
    <td><img src="img/ocnet_0000465.png" width="100%" align="center"></td>
    <td><img src="img/dunet_0000465.png" width="100%" align="center"></td>
    <td><img src="img/hrnet_0000465.png" width="100%" align="center"></td>
    <td><img src="img/hrnet+ocr_0000465.png" width="100%" align="center"></td>
    <td><img src="img/phgmm_0000465.png" width="100%" align="center"></td>
  </tr>
  <tr>
    <td align="center">Image</td>
    <td align="center">GT</td>
    <td align="center">OCNet</td>
    <td align="center">DUNet</td>
    <td align="center">HRNet</td>
    <td align="center">HRNet+OCR</td>
    <td align="center">PHGMM</td>
  </tr>
</table>
</div>




### Citations

If you find the code useful for your research, please consider citing our paper:

```
@InProceedings{Saire2022,
  author  = {Saire, D. and Ram\'irez Rivera, A.},
  title   = {Global and Local Features through Gaussian Mixture Models on Image Semantic Segmentation},
  journal={IEEE Access}, 
  year={2022},
  volume={10},
  number={},
  pages={77323-77336},
  code    = {https://gitlab.com/mipl/phgmm},
  doi     = {10.1109/ACCESS.2022.3192605},
}
```



### Setting up

- Get this repo, git clone https://gitlab.com/mipl/phgmm
- Get a docker env with tensorflow 1.9, sklearn, numpy, opencv3, python3, for example, [the tensorflow dockerfile](https://gitlab.com/mipl/phgmm/-/blob/master/docker/tf.Dockerfile) or use the [autobuild image](https://hub.docker.com/repository/docker/mipl/phgmm).  The container we are making available contains the needed dependencies, and exposes a jupyter installation (default from tensorflow), exposed on ports 8888.  Similarly, the port 6006 is exposed for tensorboard.

##### Training Params
```
    gpu_id: id of gpu to be used
    model: name of the model
    num_classes: number of classes (including void, label id:0)
    dataset: name of the dataset used
    checkpoint: path to save model
    train_data: path to dataset .tfrecords
    batch_size: training batch size
    skip_step: how many steps to print loss 
    height: height of input image
    width: width of input image
    epochs: number of epochs
    max_iteration: how many iterations to train
    learning_rate: initial learning rate
    save_step: how many steps to save the model
    power: parameter for poly learning rate
    unlabel: id of background on the dataset
    is_board: (True/False) if tensorboard is used
    tb_logs: path to save the tensorboard metrics
    load_param: (True/False) if load the parameters of the model
    path_param: path to load model
```

##### Evaluation Params
```
    gpu_id: id of gpu to be used
    model: name of the model
    num_classes: number of classes (including void, label id:0)
    dataset: name of the dataset used
    checkpoint: path to load the model
    test_data: path to dataset .tfrecords
    batch_size: evaluation batch size
    skip_step: how many steps to print mIoU
    height: height of input image
    width: width of input image
    unlabel: id of background on the dataset
    saved_results: path to saved the results
```

### Training and Evaluation

`dataset: 'cityscape_11','synthia'` 

##### Training Procedure

Edit the config file for training in config folder. RGun:

```
python3 train_PHGMM_multigpu.py --config config/train.config
```

##### Evaluation Procedure

Select a checkpoint to test/validate your model in terms of the mean IoU metric.
Edit the config file for evaluation in config folder. Run:

```
python3 evaluate_PHGMM.py --config config/test.config
```

