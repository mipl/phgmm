##### from https://github.com/raghudeep/netwarp_public/blob/master/scripts/compute_scores.py

import numpy as np
import os
import sys
#import numba
from PIL import Image
import scipy.io
from skimage.segmentation import find_boundaries
from skimage.morphology import erosion, dilation, disk, square
from tqdm import tqdm

model_set = ['phgmm', 'hrnet', 'hrnet+ocr', 'adapnet_pp', 'dunet', 'ocnet']#'danet',
sample_set = ['0000099.png', '0000465.png']
#dataset = 'synthia'

for sample in sample_set:
  print(sample)
  img_cmd = 'cp ../img/'+sample+' ./img_'+sample
  os.system(img_cmd)
  gt_cmd = 'cp ../seg_gt/'+sample+' ./gt_'+sample
  os.system(gt_cmd)
  for model in model_set:
    model_cmd =  'cp ../'+model+'/seg_pred/'+sample+' ./'+model+'_'+sample
    os.system(model_cmd)
    if model == 'phgmm': continue
    comp_name = 'phgmm-vs-'+model
    compare_cmd = 'cp ../compare-gt-pred/'+comp_name+'/diff/'+sample+' ./'+comp_name+'_'+sample
    os.system(compare_cmd)


